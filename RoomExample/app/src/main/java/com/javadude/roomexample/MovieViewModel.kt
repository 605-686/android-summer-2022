package com.javadude.roomexample

import android.app.Application
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.AndroidViewModel
import com.javadude.roomexample.data.Actor
import com.javadude.roomexample.data.ActorWithRoles
import com.javadude.roomexample.data.Movie
import com.javadude.roomexample.data.MovieWithRoles
import com.javadude.roomexample.data.Rating
import com.javadude.roomexample.data.RatingWithMovies

sealed interface Screen
object MainScreen: Screen
object RatingsScreen: Screen
object MoviesScreen: Screen
object ActorsScreen: Screen
object RatingScreen: Screen
object MovieScreen: Screen
object ActorScreen: Screen


class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: MovieRepository = MovieDatabaseRepository(application)

    var rating by mutableStateOf<RatingWithMovies?>(null)
        private set
    var movie by mutableStateOf<MovieWithRoles?>(null)
        private set
    var actor by mutableStateOf<ActorWithRoles?>(null)
        private set

    var screen by mutableStateOf<Screen?>(MainScreen)
        private set

    private var screenStack = listOf<Screen>(MainScreen)
        set(value) {
            field = value
            screen = value.lastOrNull()
        }

    fun push(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun pop() {
        screenStack = screenStack.dropLast(1)
    }

    val ratingsFlow = repository.ratingsFlow
    val moviesFlow = repository.moviesFlow
    val actorsFlow = repository.actorsFlow

    suspend fun select(rating: Rating) {
        this.rating = repository.expand(rating)
    }
    suspend fun select(movie: Movie) {
        this.movie = repository.expand(movie)
    }
    suspend fun select(actor: Actor) {
        this.actor = repository.expand(actor)
    }

    suspend fun resetDatabase() = repository.resetDatabase()
}