package com.javadude.movies2.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies2.Display
import com.javadude.movies2.Label
import com.javadude.movies2.R
import com.javadude.movies2.Screen
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.Movie
import com.javadude.movies2.data.MovieWithRoles
import com.javadude.movies2.immutableListOf
import com.javadude.movies2.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun MovieScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    movieWithRoles: MovieWithRoles?,
    select: (Actor) -> Unit,
    onEdit: (Movie) -> Unit,
) {
    requireNotNull(movieWithRoles)

    MovieScaffold(
        scope = scope,
        title = movieWithRoles.movie.title,
        topActions = immutableListOf(
            TopAction(
                icon = Icons.Default.Edit,
                contentDescriptionId = R.string.action_description_edit_movie,
            ) {
                onEdit(movieWithRoles.movie)
            }
        ),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            Label(labelId = R.string.label_description)
            Display(text = movieWithRoles.movie.title)

            Label(label = stringResource(id = R.string.label_cast_of_movie, movieWithRoles.movie.title))

            movieWithRoles.roles
                .forEach {
                    Display(text = it.name, modifier = Modifier.clickable { select(it) })
                }
        }
    }
}