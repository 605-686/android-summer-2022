package com.javadude.movies2

import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.ActorWithRoles
import com.javadude.movies2.data.Movie
import com.javadude.movies2.data.MovieWithRoles
import com.javadude.movies2.data.Rating
import com.javadude.movies2.data.RatingWithMovies
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val ratingsFlow: Flow<List<Rating>>
    val moviesFlow: Flow<List<Movie>>
    val actorsFlow: Flow<List<Actor>>

    suspend fun expand(rating: Rating): RatingWithMovies
    suspend fun expand(movie: Movie): MovieWithRoles
    suspend fun expand(actor: Actor): ActorWithRoles

    suspend fun update(movie: Movie)
    suspend fun update(actor: Actor)
    suspend fun update(rating: Rating)

    suspend fun deleteMoviesByIds(ids: List<String>)
    suspend fun deleteActorsByIds(ids: List<String>)
    suspend fun deleteRatingsByIds(ids: List<String>)

    suspend fun resetDatabase()
}