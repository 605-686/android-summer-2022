package com.javadude.movies2

import android.app.Application
import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Emergency
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.lifecycle.AndroidViewModel
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.ActorWithRoles
import com.javadude.movies2.data.Movie
import com.javadude.movies2.data.MovieWithRoles
import com.javadude.movies2.data.Rating
import com.javadude.movies2.data.RatingWithMovies
import kotlinx.coroutines.flow.map

sealed class Screen(
    @StringRes val titleId: Int,
    val icon: ImageVector,
)
object RatingsScreen: Screen(
    titleId = R.string.screen_title_ratings,
    icon = Icons.Default.Emergency,
)
object MoviesScreen: Screen(
    titleId = R.string.screen_title_movies,
    icon = Icons.Default.Movie,
)
object ActorsScreen: Screen(
    titleId = R.string.screen_title_actors,
    icon = Icons.Default.Person,
)
object RatingScreen: Screen(
    titleId = R.string.screen_title_rating,
    icon = Icons.Default.Emergency,
)
object MovieScreen: Screen(
    titleId = R.string.screen_title_movie,
    icon = Icons.Default.Movie,
)
object MovieEditScreen: Screen(
    titleId = R.string.screen_title_movie_edit,
    icon = Icons.Default.Movie,
)
object ActorScreen: Screen(
    titleId = R.string.screen_title_actor,
    icon = Icons.Default.Person,
)

val screenTargets = immutableListOf(RatingsScreen, MoviesScreen, ActorsScreen)

class MovieViewModel(application: Application) : AndroidViewModel(application) {
    private val repository: MovieRepository = MovieDatabaseRepository(application)

    var rating by mutableStateOf<RatingWithMovies?>(null)
        private set
    var movie by mutableStateOf<MovieWithRoles?>(null)
        private set
    var actor by mutableStateOf<ActorWithRoles?>(null)
        private set

    var selectedActorIds by mutableStateOf<ImmutableSet<String>>(emptyImmutableSet())
        private set
    var selectedMovieIds by mutableStateOf<ImmutableSet<String>>(emptyImmutableSet())
        private set
    var selectedRatingIds by mutableStateOf<ImmutableSet<String>>(emptyImmutableSet())
        private set

    var screen by mutableStateOf<Screen?>(MoviesScreen)
        private set

    private var screenStack = listOf<Screen>(MoviesScreen)
        set(value) {
            field = value
            screen = value.lastOrNull()
        }

    fun push(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun pop() {
        screenStack = screenStack.dropLast(1)
    }
    fun selectListScreen(screen: Screen) {
        screenStack = listOf(screen)
    }

    suspend fun update(movie: Movie) {
        repository.update(movie)
        select(movie)
    }

    val ratingsFlow = repository.ratingsFlow.map {
        ImmutableList(it)
    }
    val moviesFlow = repository.moviesFlow.map {
        ImmutableList(it)
    }
    val actorsFlow = repository.actorsFlow.map {
        ImmutableList(it)
    }

    suspend fun select(rating: Rating) {
        this.rating = repository.expand(rating)
    }
    suspend fun select(movie: Movie) {
        this.movie = repository.expand(movie)
    }
    suspend fun select(actor: Actor) {
        this.actor = repository.expand(actor)
    }

    suspend fun resetDatabase() = repository.resetDatabase()

    private fun ImmutableSet<String>.toggleSelectionId(id: String): ImmutableSet<String> =
        if (id in this)
            this - id
        else
            this + id

    fun toggleSelectedMovieId(id: String) {
        selectedMovieIds = selectedMovieIds.toggleSelectionId(id)
    }
    fun toggleSelectedActorId(id: String) {
        selectedActorIds = selectedActorIds.toggleSelectionId(id)
    }
    fun toggleSelectedRatingId(id: String) {
        selectedRatingIds = selectedRatingIds.toggleSelectionId(id)
    }

    fun clearSelectedActorIds() {
        selectedActorIds = emptyImmutableSet()
    }
    fun clearSelectedMovieIds() {
        selectedMovieIds = emptyImmutableSet()
    }
    fun clearSelectedRatingIds() {
        selectedRatingIds = emptyImmutableSet()
    }

    suspend fun deleteSelectedActors() {
        repository.deleteActorsByIds(selectedActorIds.toList())
        clearSelectedActorIds()
    }
    suspend fun deleteSelectedMovies() {
        repository.deleteMoviesByIds(selectedMovieIds.toList())
        clearSelectedMovieIds()
    }
    suspend fun deleteSelectedRatings() {
        repository.deleteRatingsByIds(selectedRatingIds.toList())
        clearSelectedRatingIds()
    }
}