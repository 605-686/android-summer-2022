package com.javadude.movies2.screens

import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.javadude.movies2.ImmutableList
import com.javadude.movies2.ImmutableSet
import com.javadude.movies2.R
import com.javadude.movies2.Screen
import com.javadude.movies2.data.Actor
import com.javadude.movies2.immutableListOf
import com.javadude.movies2.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun ActorsScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    actors: ImmutableList<Actor>,
    onReset: () -> Unit,
    onToggleSelect: (String) -> Unit,
    selectedIds: ImmutableSet<String>,
    onDeleteSelections: suspend () -> Unit,
    onClearSelections: () -> Unit,
    select: (Actor) -> Unit,
) =
    ListScaffold(
        scope = scope,
        title = stringResource(id = R.string.screen_title_actors),
        topActions = immutableListOf(
            TopAction(
                icon = Icons.Default.Refresh,
                contentDescriptionId = R.string.action_reset_database,
                onClick = onReset,
            )
        ),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
        items = actors,
        getKey = { actor -> actor.id },
        onItemClick = select,
        onToggleSelect = onToggleSelect,
        selectedIds = selectedIds,
        icon = Icons.Default.Person,
        onClearSelections = onClearSelections,
        onDeleteSelections = onDeleteSelections,
    ) { actor, modifier ->
        Text(text = actor.name, modifier = modifier)
    }
