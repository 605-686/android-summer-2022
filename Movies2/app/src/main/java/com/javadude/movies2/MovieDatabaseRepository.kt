package com.javadude.movies2

import android.app.Application
import androidx.room.Room
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.ActorWithRoles
import com.javadude.movies2.data.Movie
import com.javadude.movies2.data.MovieDatabase
import com.javadude.movies2.data.MovieWithRoles
import com.javadude.movies2.data.Rating
import com.javadude.movies2.data.RatingWithMovies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieDatabaseRepository(private val application: Application): MovieRepository {
    private val db =
        Room.databaseBuilder(
            application,
            MovieDatabase::class.java,
            "MOVIES"
        ).build()

    override val ratingsFlow = db.dao().getRatingsFlow()
    override val moviesFlow = db.dao().getMoviesFlow()
    override val actorsFlow = db.dao().getActorsFlow()

    override suspend fun expand(rating: Rating): RatingWithMovies = withContext(Dispatchers.IO) {
        db.dao().getRatingWithMovies(rating.id)
    }
    override suspend fun expand(movie: Movie): MovieWithRoles = withContext(Dispatchers.IO) {
        db.dao().getMovieWithRoles(movie.id)
    }
    override suspend fun expand(actor: Actor): ActorWithRoles = withContext(Dispatchers.IO) {
        db.dao().getActorWithRoles(actor.id)
    }

    override suspend fun update(movie: Movie) = withContext(Dispatchers.IO) {
        db.dao().update(movie)
    }

    override suspend fun update(actor: Actor) {
        db.dao().update(actor)
    }

    override suspend fun update(rating: Rating) {
        db.dao().update(rating)
    }

    override suspend fun resetDatabase() = withContext(Dispatchers.IO) {
        db.dao().resetDatabase()
    }

    override suspend fun deleteMoviesByIds(ids: List<String>) = withContext(Dispatchers.IO) {
        db.dao().deleteMoviesByIds(ids)
    }
    override suspend fun deleteActorsByIds(ids: List<String>) = withContext(Dispatchers.IO) {
        db.dao().deleteActorsByIds(ids)
    }
    override suspend fun deleteRatingsByIds(ids: List<String>) = withContext(Dispatchers.IO) {
        db.dao().deleteRatingsByIds(ids)
    }
}