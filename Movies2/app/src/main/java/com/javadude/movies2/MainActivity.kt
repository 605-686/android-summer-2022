package com.javadude.movies2

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import com.javadude.movies2.screens.ActorScreen
import com.javadude.movies2.screens.ActorsScreen
import com.javadude.movies2.screens.MovieEditScreen
import com.javadude.movies2.screens.MovieScreen
import com.javadude.movies2.screens.MoviesScreen
import com.javadude.movies2.screens.RatingScreen
import com.javadude.movies2.screens.RatingsScreen
import com.javadude.movies2.ui.theme.Movies2Theme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val scope = rememberCoroutineScope()

            BackHandler {
                viewModel.pop()
            }

            Movies2Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Ui(scope, viewModel) { finish() }
                }
            }
        }
    }
}

@Composable
fun Ui(
    scope: CoroutineScope,
    viewModel: MovieViewModel,
    exit: () -> Unit,
) {
    val ratings by viewModel.ratingsFlow.collectAsState(initial = emptyImmutableList())
    val movies by viewModel.moviesFlow.collectAsState(initial = emptyImmutableList())
    val actors by viewModel.actorsFlow.collectAsState(initial = emptyImmutableList())

    when (val currentScreen = viewModel.screen) {
        null -> exit()
        ActorsScreen -> ActorsScreen(
            scope = scope,
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            actors = actors,
            onReset = {
                scope.launch {
                    viewModel.resetDatabase()
                }
            },
            selectedIds = viewModel.selectedActorIds,
            onToggleSelect = viewModel::toggleSelectedActorId,
            onDeleteSelections = viewModel::deleteSelectedActors,
            onClearSelections = viewModel::clearSelectedActorIds,
        ) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(ActorScreen)
            }
        }
        MoviesScreen -> MoviesScreen(
            scope = scope,
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            movies = movies,
            onReset = {
                scope.launch {
                    viewModel.resetDatabase()
                }
            },
            selectedIds = viewModel.selectedMovieIds,
            onToggleSelect = viewModel::toggleSelectedMovieId,
            onDeleteSelections = viewModel::deleteSelectedMovies,
            onClearSelections = viewModel::clearSelectedMovieIds,
        ) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(MovieScreen)
            }
        }
        RatingsScreen -> RatingsScreen(
            scope = scope,
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            ratings = ratings,
            onReset = {
                scope.launch {
                    viewModel.resetDatabase()
                }
            },
            selectedIds = viewModel.selectedRatingIds,
            onToggleSelect = viewModel::toggleSelectedRatingId,
            onDeleteSelections = viewModel::deleteSelectedRatings,
            onClearSelections = viewModel::clearSelectedRatingIds,
        ) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(RatingScreen)
            }
        }
        ActorScreen -> ActorScreen(
            scope = scope,
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            actorWithRoles = viewModel.actor,
        ) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(MovieScreen)
            }
        }
        MovieScreen -> MovieScreen(
            scope = scope,
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            movieWithRoles = viewModel.movie,
            onEdit = {
                viewModel.push(MovieEditScreen)
            },
            select = {
                scope.launch {
                    viewModel.select(it)
                    viewModel.push(ActorScreen)
                }
            }
        )
        MovieEditScreen -> MovieEditScreen(
            scope = scope,
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            movieWithRoles = viewModel.movie,
        ) {
            scope.launch {
                viewModel.update(it)
            }
        }
        RatingScreen -> RatingScreen(
            scope = scope,
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            ratingWithMovies = viewModel.rating,
        ) {
            scope.launch {
                viewModel.select(it)
                viewModel.push(MovieScreen)
            }
        }
    }
}

