package com.javadude.movies4.repo

import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val ratingsFlow: Flow<List<RatingDto>>
    val moviesFlow: Flow<List<MovieDto>>
    val actorsFlow: Flow<List<ActorDto>>

    suspend fun getRatingWithMovies(id: String): RatingWithMoviesDto
    suspend fun getMovieWithRoles(id: String): MovieWithRolesDto
    suspend fun getActorWithRoles(id: String): ActorWithRolesDto

    suspend fun update(movie: MovieDto)
    suspend fun update(actor: ActorDto)
    suspend fun update(rating: RatingDto)

    suspend fun deleteMoviesByIds(ids: List<String>)
    suspend fun deleteActorsByIds(ids: List<String>)
    suspend fun deleteRatingsByIds(ids: List<String>)

    suspend fun resetDatabase()
}