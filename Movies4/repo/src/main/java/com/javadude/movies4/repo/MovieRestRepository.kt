package com.javadude.movies4.repo

import com.javadude.movies4.data.MovieApiService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Response

class MovieRestRepository(private val coroutineScope: CoroutineScope): MovieRepository {
    inner class ListFlowManager<T>(
        private val fetcher: suspend () -> Response<List<T>>
    ) {
        private val _flow = MutableStateFlow(emptyList<T>())
        init {
            fetch()
        }
        val flow: Flow<List<T>>
            get() = _flow

        fun fetch() =
            coroutineScope.launch(Dispatchers.IO) {
                _flow.value = fetcher().takeIf { it.isSuccessful }?.body() ?: emptyList()
            }
    }

    private val movieApiService = MovieApiService.create()

    private val ratingsFlowManager = ListFlowManager { movieApiService.getRatings() }
    private val moviesFlowManager = ListFlowManager { movieApiService.getMovies() }
    private val actorsFlowManager = ListFlowManager { movieApiService.getActors() }

    override val ratingsFlow = ratingsFlowManager.flow.map { ImmutableList(it.map { rating -> rating.toDto()}) }
    override val moviesFlow = moviesFlowManager.flow.map { ImmutableList(it.map { movie -> movie.toDto()}) }
    override val actorsFlow = actorsFlowManager.flow.map { ImmutableList(it.map { actor -> actor.toDto()}) }

    suspend fun <EntityType, DtoType> get(type: String, id: String, toDto: EntityType.() -> DtoType, getter: suspend (String) -> Response<EntityType>): DtoType = withContext(Dispatchers.IO) {
        val response = getter(id)
        if (!response.isSuccessful) {
            throw IllegalStateException("Error retrieving $type with id $id: ${response.message()}")
        }
        response.body()?.toDto()
            ?: throw IllegalStateException("No actor returned for id $id: ${response.message()}")
    }

    override suspend fun getActorWithRoles(id: String) =
        get(
            type = "actor",
            id = id,
            toDto = { toDto() },
            getter = { movieApiService.getActorWithRoles(it) },
        )

    override suspend fun getMovieWithRoles(id: String) =
        get(
            type = "movie",
            id = id,
            toDto = { toDto() },
            getter = { movieApiService.getMovieWithRoles(it) },
        )

    override suspend fun getRatingWithMovies(id: String) =
        get(
            type = "rating",
            id = id,
            toDto = { toDto() },
            getter = { movieApiService.getRatingWithMovies(it) },
        )

    override suspend fun update(movie: MovieDto) {
        withContext(Dispatchers.IO) {
            movieApiService.updateMovie(movie.id, movie.toEntity())
            moviesFlowManager.fetch()
        }
    }

    override suspend fun update(actor: ActorDto) {
        withContext(Dispatchers.IO) {
            movieApiService.updateActor(actor.id, actor.toEntity())
            actorsFlowManager.fetch()
        }
    }

    override suspend fun update(rating: RatingDto) {
        withContext(Dispatchers.IO) {
            movieApiService.updateRating(rating.id, rating.toEntity())
            ratingsFlowManager.fetch()
        }
    }

    override suspend fun deleteMoviesByIds(ids: List<String>) {
        withContext(Dispatchers.IO) {
            ids.forEach {
                movieApiService.deleteMovie(it)
            }
            moviesFlowManager.fetch()
        }
    }

    override suspend fun deleteActorsByIds(ids: List<String>) {
        withContext(Dispatchers.IO) {
            ids.forEach {
                movieApiService.deleteActor(it)
            }
            actorsFlowManager.fetch()
        }
    }

    override suspend fun deleteRatingsByIds(ids: List<String>) {
        withContext(Dispatchers.IO) {
            ids.forEach {
                movieApiService.deleteRating(it)
            }
            ratingsFlowManager.fetch()
        }
    }

    override suspend fun resetDatabase() {
        withContext(Dispatchers.IO) {
            movieApiService.resetDatabase()
        }
        moviesFlowManager.fetch()
        actorsFlowManager.fetch()
        ratingsFlowManager.fetch()
    }
}