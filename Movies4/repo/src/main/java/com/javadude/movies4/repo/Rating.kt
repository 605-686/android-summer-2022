package com.javadude.movies4.repo

import androidx.compose.runtime.Immutable
import com.javadude.movies4.data.Rating
import com.javadude.movies4.data.RatingWithMovies

@Immutable
data class RatingDto(
    val id: String,
    val name: String,
    val description: String,
)

@Immutable
data class RatingWithMoviesDto(
    val rating: RatingDto,
    val movies: List<MovieDto>
)

fun Rating.toDto() =
    RatingDto(id = id, name = name, description = description)
fun RatingDto.toEntity() =
    Rating(id = id, name = name, description = description)

fun RatingWithMovies.toDto() =
    RatingWithMoviesDto(
        rating = rating.toDto(),
        movies =
            ImmutableList(
                movies.map {
                    it.toDto()
                }
            )
    )