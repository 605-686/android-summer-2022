package com.javadude.movies4.repo

import androidx.compose.runtime.Immutable
import com.javadude.movies4.data.Actor
import com.javadude.movies4.data.ActorWithRoles

@Immutable
data class ActorDto(
    val id: String,
    val name: String,
)

@Immutable
data class ActorWithRolesDto(
    val actor: ActorDto,
    val roles: ImmutableList<MovieDto>,
)

fun Actor.toDto() =
    ActorDto(id = id, name = name)
fun ActorDto.toEntity() =
    Actor(id = id, name = name)

fun ActorWithRoles.toDto() =
    ActorWithRolesDto(
        actor = actor.toDto(),
        roles =
            ImmutableList(
                roles.map {
                    it.toDto()
                }
            )
    )