package com.javadude.movies4.repo

import androidx.compose.runtime.Immutable
import com.javadude.movies4.data.ActorWithRoles
import com.javadude.movies4.data.Movie
import com.javadude.movies4.data.MovieWithRoles

@Immutable
data class MovieDto(
    val id: String,
    val title: String,
    val description: String,
    val ratingId: String,
)

@Immutable
data class MovieWithRolesDto(
    val movie: MovieDto,
    val roles: List<ActorDto>
)

fun Movie.toDto() =
    MovieDto(id = id, title = title, description = description, ratingId = ratingId)
fun MovieDto.toEntity() =
    Movie(id = id, title = title, description = description, ratingId = ratingId)

fun MovieWithRoles.toDto() =
    MovieWithRolesDto(
        movie = movie.toDto(),
        roles =
            ImmutableList(
                roles.map {
                    it.toDto()
                }
            )
    )