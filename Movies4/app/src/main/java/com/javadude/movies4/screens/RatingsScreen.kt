package com.javadude.movies4.screens

import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Emergency
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.javadude.movies4.ImmutableSet
import com.javadude.movies4.R
import com.javadude.movies4.Screen
import com.javadude.movies4.repo.ImmutableList
import com.javadude.movies4.repo.RatingDto
import com.javadude.movies4.repo.immutableListOf
import com.javadude.movies4.screenTargets

@Composable
fun RatingsScreen(
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    ratings: ImmutableList<RatingDto>,
    onReset: () -> Unit,
    onToggleSelect: (String) -> Unit,
    selectedIds: ImmutableSet<String>,
    onDeleteSelections: () -> Unit,
    onClearSelections: () -> Unit,
    select: (String) -> Unit,
) =
    ListScaffold(
        title = stringResource(id = R.string.screen_title_ratings),
        topActions = immutableListOf(
            TopAction(
                icon = Icons.Default.Refresh,
                contentDescriptionId = R.string.screen_title_ratings,
                onClick = onReset,
            )
        ),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
        items = ratings,
        getKey = { rating -> rating.id },
        onItemClick = select,
        onToggleSelect = onToggleSelect,
        selectedIds = selectedIds,
        icon = Icons.Default.Emergency,
        onClearSelections = onClearSelections,
        onDeleteSelections = onDeleteSelections,
    ) { rating, modifier ->
        Text(text = rating.name, modifier = modifier)
    }
