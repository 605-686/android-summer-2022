package com.javadude.movies4.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies4.Display
import com.javadude.movies4.Label
import com.javadude.movies4.R
import com.javadude.movies4.Screen
import com.javadude.movies4.repo.MovieWithRolesDto
import com.javadude.movies4.repo.emptyImmutableList
import com.javadude.movies4.repo.immutableListOf
import com.javadude.movies4.screenTargets

@Composable
fun MovieScreen(
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    id: String,
    fetchMovie: suspend (String) -> MovieWithRolesDto,
    select: (String) -> Unit,
    onEdit: (String) -> Unit,
) {
    var movieWithRoles by remember {
        mutableStateOf<MovieWithRolesDto?>(null)
    }

    LaunchedEffect(id) {
        movieWithRoles = fetchMovie(id)
    }

    MovieScaffold(
        title = movieWithRoles?.movie?.title ?: stringResource(id = R.string.loading),
        topActions =
        movieWithRoles?.let {
            immutableListOf(
                TopAction(
                    icon = Icons.Default.Edit,
                    contentDescriptionId = R.string.action_description_edit_movie,
                ) {
                    onEdit(it.movie.id)
                }
            )
        } ?: emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            Label(labelId = R.string.label_description)
            Display(text = movieWithRoles?.movie?.title ?: "")

            movieWithRoles?.let { movieWithRoles ->
                Label(label = stringResource(id = R.string.label_cast_of_movie, movieWithRoles.movie.title))

                movieWithRoles.roles
                    .forEach {
                        Display(text = it.name, modifier = Modifier.clickable { select(it.id) })
                    }
            }
        }
    }
}