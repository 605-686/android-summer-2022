package com.javadude.movies4.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies4.Display
import com.javadude.movies4.Label
import com.javadude.movies4.R
import com.javadude.movies4.Screen
import com.javadude.movies4.repo.ActorWithRolesDto
import com.javadude.movies4.repo.emptyImmutableList
import com.javadude.movies4.screenTargets

@Composable
fun ActorScreen(
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    id: String,
    fetchActor: suspend (String) -> ActorWithRolesDto,
    select: (String) -> Unit,
) {
    var actorWithRoles by remember {
        mutableStateOf<ActorWithRolesDto?>(null)
    }

    LaunchedEffect(id) {
        actorWithRoles = fetchActor(id)
    }

    MovieScaffold(
        title = actorWithRoles?.actor?.name ?: stringResource(id = R.string.screen_title_actor),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            actorWithRoles?.let { actorWithRoles ->
                Label(label = stringResource(id = R.string.label_movies_starring_actor, actorWithRoles.actor.name))

                actorWithRoles.roles
                    .forEach {
                        Display(text = it.title, modifier = Modifier.clickable { select(it.id) })
                    }
            }
        }
    }
}