package com.javadude.movies4.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies4.Display
import com.javadude.movies4.Label
import com.javadude.movies4.R
import com.javadude.movies4.Screen
import com.javadude.movies4.repo.RatingWithMoviesDto
import com.javadude.movies4.repo.emptyImmutableList
import com.javadude.movies4.screenTargets

@Composable
fun RatingScreen(
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    id: String,
    fetchRating: suspend (String) -> RatingWithMoviesDto,
    select: (String) -> Unit,
) {
    var ratingWithMovies by remember {
        mutableStateOf<RatingWithMoviesDto?>(null)
    }

    LaunchedEffect(id) {
        ratingWithMovies = fetchRating(id)
    }

    MovieScaffold(
        title = ratingWithMovies?.rating?.name ?: stringResource(id = R.string.loading),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            Label(labelId = R.string.label_description)
            Display(text = ratingWithMovies?.rating?.description ?: "")

            ratingWithMovies?.let { ratingWithMovies ->
                Label(
                    label = stringResource(
                        id = R.string.label_movies_with_rating,
                        ratingWithMovies.rating.name
                    )
                )
                ratingWithMovies.movies.forEach {
                    Display(text = it.title, modifier = Modifier.clickable { select(it.id) })
                }
            }
        }
    }
}

