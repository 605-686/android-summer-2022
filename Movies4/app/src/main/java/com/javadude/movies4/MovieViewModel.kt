package com.javadude.movies4

import android.app.Application
import androidx.annotation.StringRes
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Emergency
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.javadude.movies4.repo.ImmutableList
import com.javadude.movies4.repo.MovieDto
import com.javadude.movies4.repo.MovieRepository
import com.javadude.movies4.repo.MovieRestRepository
import com.javadude.movies4.repo.immutableListOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

sealed class Screen(
    @StringRes val titleId: Int,
    val icon: ImageVector,
)
object RatingsScreen: Screen(
    titleId = R.string.screen_title_ratings,
    icon = Icons.Default.Emergency,
)
object MoviesScreen: Screen(
    titleId = R.string.screen_title_movies,
    icon = Icons.Default.Movie,
)
object ActorsScreen: Screen(
    titleId = R.string.screen_title_actors,
    icon = Icons.Default.Person,
)
data class RatingScreen(val id: String): Screen(
    titleId = R.string.screen_title_rating,
    icon = Icons.Default.Emergency,
)
data class MovieScreen(val id: String): Screen(
    titleId = R.string.screen_title_movie,
    icon = Icons.Default.Movie,
)
data class MovieEditScreen(val id: String): Screen(
    titleId = R.string.screen_title_movie_edit,
    icon = Icons.Default.Movie,
)
data class ActorScreen(val id: String): Screen(
    titleId = R.string.screen_title_actor,
    icon = Icons.Default.Person,
)

val screenTargets = immutableListOf(RatingsScreen, MoviesScreen, ActorsScreen)

class MovieViewModel(application: Application) : AndroidViewModel(application) {
//    private val repository: MovieRepository = MovieDatabaseRepository(application)
    private val repository: MovieRepository = MovieRestRepository(viewModelScope)

    var selectedActorIds by mutableStateOf<ImmutableSet<String>>(emptyImmutableSet())
        private set
    var selectedMovieIds by mutableStateOf<ImmutableSet<String>>(emptyImmutableSet())
        private set
    var selectedRatingIds by mutableStateOf<ImmutableSet<String>>(emptyImmutableSet())
        private set

    var screen by mutableStateOf<Screen?>(MoviesScreen)
        private set

    private var screenStack = listOf<Screen>(MoviesScreen)
        set(value) {
            field = value
            screen = value.lastOrNull()
        }

    fun push(screen: Screen) {
        screenStack = screenStack + screen
    }
    fun pop() {
        screenStack = screenStack.dropLast(1)
    }
    fun selectListScreen(screen: Screen) {
        screenStack = listOf(screen)
    }

    fun update(movie: MovieDto) {
        viewModelScope.launch {
            repository.update(movie)
        }
    }

    val ratingsFlow = repository.ratingsFlow.map {
        ImmutableList(it)
    }
    val moviesFlow = repository.moviesFlow.map {
        ImmutableList(it)
    }
    val actorsFlow = repository.actorsFlow.map {
        ImmutableList(it)
    }

    fun resetDatabase() {
        viewModelScope.launch {
            repository.resetDatabase()
        }
    }

    private fun ImmutableSet<String>.toggleSelectionId(id: String): ImmutableSet<String> =
        if (id in this)
            this - id
        else
            this + id

    fun toggleSelectedMovieId(id: String) {
        selectedMovieIds = selectedMovieIds.toggleSelectionId(id)
    }
    fun toggleSelectedActorId(id: String) {
        selectedActorIds = selectedActorIds.toggleSelectionId(id)
    }
    fun toggleSelectedRatingId(id: String) {
        selectedRatingIds = selectedRatingIds.toggleSelectionId(id)
    }

    fun clearSelectedActorIds() {
        selectedActorIds = emptyImmutableSet()
    }
    fun clearSelectedMovieIds() {
        selectedMovieIds = emptyImmutableSet()
    }
    fun clearSelectedRatingIds() {
        selectedRatingIds = emptyImmutableSet()
    }

    fun deleteSelectedActors() {
        viewModelScope.launch {
            repository.deleteActorsByIds(selectedActorIds.toList())
            clearSelectedActorIds()
        }
    }
    fun deleteSelectedMovies() {
        viewModelScope.launch {
            repository.deleteMoviesByIds(selectedMovieIds.toList())
            clearSelectedMovieIds()
        }
    }
    fun deleteSelectedRatings() {
        viewModelScope.launch {
            repository.deleteRatingsByIds(selectedRatingIds.toList())
            clearSelectedRatingIds()
        }
    }

    suspend fun getMovieWithRoles(id: String) =
        repository.getMovieWithRoles(id)
    suspend fun getActorWithRoles(id: String) =
        repository.getActorWithRoles(id)
    suspend fun getRatingWithMovies(id: String) =
        repository.getRatingWithMovies(id)
}