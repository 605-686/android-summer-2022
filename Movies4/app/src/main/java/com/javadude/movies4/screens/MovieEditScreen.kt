package com.javadude.movies4.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.javadude.movies4.R
import com.javadude.movies4.Screen
import com.javadude.movies4.TextField
import com.javadude.movies4.repo.MovieDto
import com.javadude.movies4.repo.MovieWithRolesDto
import com.javadude.movies4.repo.emptyImmutableList
import com.javadude.movies4.screenTargets

@Composable
fun MovieEditScreen(
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    id: String,
    fetchMovie: suspend (String) -> MovieWithRolesDto,
    onMovieChange: (MovieDto) -> Unit,
) {
    var movieWithRoles by remember {
        mutableStateOf<MovieWithRolesDto?>(null)
    }

    LaunchedEffect(id) {
        movieWithRoles = fetchMovie(id)
    }

    var title by remember(movieWithRoles) {
        mutableStateOf(movieWithRoles?.movie?.title ?: "")
    }
    var description by remember(movieWithRoles) {
        mutableStateOf(movieWithRoles?.movie?.description ?: "")
    }

    MovieScaffold(
        title = title,
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            TextField(
                labelId = R.string.label_title,
                placeholderId = R.string.placeholder_movie_title,
                value = title,
                onValueChange = {
                    movieWithRoles?.let { movieWithRoles ->
                        title = it
                        onMovieChange(movieWithRoles.movie.copy(title = it))
                    }
                }
            )
            TextField(
                labelId = R.string.label_description,
                placeholderId = R.string.placeholder_movie_description,
                value = description,
                onValueChange = {
                    movieWithRoles?.let { movieWithRoles ->
                        description = it
                        onMovieChange(movieWithRoles.movie.copy(description = it))
                    }
                }
            )
        }
    }
}