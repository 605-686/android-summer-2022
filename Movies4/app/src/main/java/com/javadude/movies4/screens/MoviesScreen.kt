package com.javadude.movies4.screens

import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Movie
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.javadude.movies4.ImmutableSet
import com.javadude.movies4.R
import com.javadude.movies4.Screen
import com.javadude.movies4.repo.ImmutableList
import com.javadude.movies4.repo.MovieDto
import com.javadude.movies4.repo.immutableListOf
import com.javadude.movies4.screenTargets

@Composable
fun MoviesScreen(
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    movies: ImmutableList<MovieDto>,
    onReset: () -> Unit,
    onToggleSelect: (String) -> Unit,
    selectedIds: ImmutableSet<String>,
    onDeleteSelections: () -> Unit,
    onClearSelections: () -> Unit,
    select: (String) -> Unit,
) =
    ListScaffold(
        title = stringResource(id = R.string.screen_title_movies),
        topActions = immutableListOf(
            TopAction(
                icon = Icons.Default.Refresh,
                contentDescriptionId = R.string.action_reset_database,
                onClick = onReset,
            )
        ),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
        items = movies,
        getKey = { movie -> movie.id },
        onItemClick = select,
        onToggleSelect = onToggleSelect,
        selectedIds = selectedIds,
        icon = Icons.Default.Movie,
        onDeleteSelections = onDeleteSelections,
        onClearSelections = onClearSelections,
    ) { movie, modifier ->
        Text(text = movie.title, modifier = modifier)
    }

