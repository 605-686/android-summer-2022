package com.javadude.movies4

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import com.javadude.movies4.repo.emptyImmutableList
import com.javadude.movies4.screens.ActorScreen
import com.javadude.movies4.screens.ActorsScreen
import com.javadude.movies4.screens.MovieEditScreen
import com.javadude.movies4.screens.MovieScreen
import com.javadude.movies4.screens.MoviesScreen
import com.javadude.movies4.screens.RatingScreen
import com.javadude.movies4.screens.RatingsScreen
import com.javadude.movies4.ui.theme.Movies4Theme

class MainActivity : ComponentActivity() {
    private val viewModel by viewModels<MovieViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BackHandler {
                viewModel.pop()
            }

            Movies4Theme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Ui(viewModel) { finish() }
                }
            }
        }
    }
}

@Composable
fun Ui(
    viewModel: MovieViewModel,
    exit: () -> Unit,
) {
    val ratings by viewModel.ratingsFlow.collectAsState(initial = emptyImmutableList())
    val movies by viewModel.moviesFlow.collectAsState(initial = emptyImmutableList())
    val actors by viewModel.actorsFlow.collectAsState(initial = emptyImmutableList())

    when (val currentScreen = viewModel.screen) {
        null -> exit()
        ActorsScreen -> ActorsScreen(
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            actors = actors,
            onReset = {
                viewModel.resetDatabase()
            },
            selectedIds = viewModel.selectedActorIds,
            onToggleSelect = viewModel::toggleSelectedActorId,
            onDeleteSelections = viewModel::deleteSelectedActors,
            onClearSelections = viewModel::clearSelectedActorIds,
        ) {
            viewModel.push(ActorScreen(it))
        }
        MoviesScreen -> MoviesScreen(
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            movies = movies,
            onReset = {
                viewModel.resetDatabase()
            },
            selectedIds = viewModel.selectedMovieIds,
            onToggleSelect = viewModel::toggleSelectedMovieId,
            onDeleteSelections = viewModel::deleteSelectedMovies,
            onClearSelections = viewModel::clearSelectedMovieIds,
        ) {
            viewModel.push(MovieScreen(it))
        }
        RatingsScreen -> RatingsScreen(
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            ratings = ratings,
            onReset = {
                viewModel.resetDatabase()
            },
            selectedIds = viewModel.selectedRatingIds,
            onToggleSelect = viewModel::toggleSelectedRatingId,
            onDeleteSelections = viewModel::deleteSelectedRatings,
            onClearSelections = viewModel::clearSelectedRatingIds,
        ) {
            viewModel.push(RatingScreen(it))
        }
        is ActorScreen -> ActorScreen(
            currentScreen = currentScreen,
            id = currentScreen.id,
            fetchActor = viewModel::getActorWithRoles,
            onScreenSelect = viewModel::selectListScreen,
        ) {
            viewModel.push(MovieScreen(it))
        }
        is MovieScreen -> MovieScreen(
            currentScreen = currentScreen,
            onScreenSelect = viewModel::selectListScreen,
            id = currentScreen.id,
            fetchMovie = viewModel::getMovieWithRoles,
            onEdit = {
                viewModel.push(MovieEditScreen(it))
            },
            select = {
                viewModel.push(ActorScreen(it))
            }
        )
        is MovieEditScreen -> MovieEditScreen(
            currentScreen = currentScreen,
            id = currentScreen.id,
            fetchMovie = viewModel::getMovieWithRoles,
            onScreenSelect = viewModel::selectListScreen,
        ) {
            viewModel.update(it)
        }
        is RatingScreen -> RatingScreen(
            currentScreen = currentScreen,
            id = currentScreen.id,
            fetchRating = viewModel::getRatingWithMovies,
            onScreenSelect = viewModel::selectListScreen,
        ) {
            viewModel.push(MovieScreen(it))
        }
    }
}

