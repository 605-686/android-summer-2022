package com.javadude.movies4.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.javadude.movies4.ImmutableSet
import com.javadude.movies4.R
import com.javadude.movies4.Screen
import com.javadude.movies4.repo.ImmutableList

@Composable
fun <T> ListScaffold(
    title: String,
    topActions: ImmutableList<TopAction>,
    currentScreen: Screen,
    screenTargets: ImmutableList<Screen>,
    onScreenSelect: (Screen) -> Unit,
    items: ImmutableList<T>,
    getKey: (T) -> String,
    onItemClick: (String) -> Unit,
    onToggleSelect: (String) -> Unit,
    selectedIds: ImmutableSet<String>,
    onDeleteSelections: () -> Unit,
    onClearSelections: () -> Unit,
    icon: ImageVector,
    itemContent: @Composable (T, Modifier) -> Unit,
) {
    MovieScaffold(
        title = title,
        topActions = topActions,
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
        numSelected = selectedIds.size,
        onDeleteSelections = onDeleteSelections,
        onClearSelections = onClearSelections,
    ) { contentModifier ->
        LazyColumn(
            modifier = contentModifier,
        ) {
            items(
                items = items,
                key = getKey,
            ) { item ->
                val key = getKey(item)
                Card(
                    elevation = 4.dp,
                    backgroundColor =
                        if (key in selectedIds)
                            MaterialTheme.colors.primary
                        else
                            MaterialTheme.colors.surface
                    ,
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                        .pointerInput(selectedIds) {
                            detectTapGestures(
                                onLongPress = {
                                    onToggleSelect(key)
                                },
                                onTap = {
                                    if (selectedIds.isNotEmpty()) {
                                        onToggleSelect(key)
                                    } else {
                                        onItemClick(key)
                                    }
                                }
                            )
                        },
                ) {
                    Row(
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Icon(
                            imageVector = icon,
                            contentDescription = stringResource(id = R.string.click_to_select),
                            modifier = Modifier
                                .size(48.dp)
                                .padding(8.dp)
                                .clickable {
                                    onToggleSelect(key)
                                }
                        )
                        itemContent(item, Modifier.padding(8.dp).weight(1f))
                    }
                }
            }
        }
    }
}