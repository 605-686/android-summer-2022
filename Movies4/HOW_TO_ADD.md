Adding Movie
    MoviesScreen (list)
        Add a "add" button to the toolbar with Icons.Default.Add icon
            When pressed start a coroutine to do all of the following
                Create instance of Movie
                    use default id = random UUID
                    use "" for title and description
                    use Not Rated for rating
                    (change app to not allow ratings to be deleted)
                Insert into DB
                Set that movie to be the selected movie
                Push MovieEditScreen
                    (user edits and presses back to return to movie list)

Adding Actors and Ratings are similar

Casting actors in movies
    In movie edit screen
        have add button next to actor
            when pressed
                bring up dialog (we'll talk about those another time)
                    search and list actors
                    tap actors to add
                        bring up another dialog to ask for character name and position in credits
                        (position in credits could just be done br drag-reorder in lists)
                    press done when finished
    In actor edit screen, similar for looking up movies
                