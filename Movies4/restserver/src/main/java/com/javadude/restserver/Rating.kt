package com.javadude.restserver

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement
data class Rating(
    @JsonProperty("id") val id: String = UUID.randomUUID().toString(),
    @JsonProperty("name") val name: String,
    @JsonProperty("description") val description: String
)

// ONE-TO-MANY relationship (Rating -> Movies)
@XmlRootElement
data class RatingWithMovies(
    @JsonProperty("rating") val rating: Rating,
    @JsonProperty("movies") val movies: List<Movie>
)