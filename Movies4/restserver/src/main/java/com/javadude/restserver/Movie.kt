package com.javadude.restserver

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.UUID
import javax.xml.bind.annotation.XmlRootElement

@XmlRootElement
data class Movie(
    @JsonProperty("id") val id: String = UUID.randomUUID().toString(),
    @JsonProperty("title") val title: String,
    @JsonProperty("description") val description: String,
    @JsonProperty("ratingId") val ratingId: String,
)

@XmlRootElement
data class MovieWithRoles(
    @JsonProperty("movie") val movie: Movie,
    @JsonProperty("roles") val roles: List<Actor>,
)