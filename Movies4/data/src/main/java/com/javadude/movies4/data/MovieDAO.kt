package com.javadude.movies4.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Update
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import kotlinx.coroutines.flow.Flow

val NotRated = Rating(id = "r0", name = "Not Rated", description = "Not yet rated")
val G = Rating(id = "r1", name = "G", description = "General Audiences")
val PG = Rating(id = "r2", name = "PG", description = "Parental Guidance Suggested")
val PG13 = Rating(id = "r3", name = "PG-13", description = "Unsuitable for those under 13")
val R = Rating(id = "r4", name = "R", description = "Restricted - 17 and older")

@Dao
abstract class MovieDAO {
    @Query("SELECT * FROM Rating")
    abstract fun getRatingsFlow(): Flow<List<Rating>>

    @Query("SELECT * FROM Movie")
    abstract fun getMoviesFlow(): Flow<List<Movie>>

    @Query("SELECT * FROM Actor")
    abstract fun getActorsFlow(): Flow<List<Actor>>

    @Transaction
    @Query("SELECT * FROM Rating WHERE id = :id")
    abstract fun getRatingWithMovies(id: String): RatingWithMovies

    @Transaction
    @Query("SELECT * FROM Movie WHERE id = :id")
    abstract fun getMovieWithRoles(id: String): MovieWithRoles

    @Transaction
    @Query("SELECT * FROM Actor WHERE id = :id")
    abstract fun getActorWithRoles(id: String): ActorWithRoles

    @Insert
    abstract fun insert(vararg ratings: Rating)
    @Insert
    abstract fun insert(vararg movies: Movie)
    @Insert
    abstract fun insert(vararg actors: Actor)
    @Insert
    abstract fun insert(vararg roles: Role)
//    @Insert
//    abstract fun insert(vararg roleDetails: RoleDetails)

    @Update
    abstract fun update(vararg ratings: Rating)
    @Update
    abstract fun update(vararg movies: Movie)
    @Update
    abstract fun update(vararg actors: Actor)
    @Update
    abstract fun update(vararg roles: Role)
//    @Update
//    abstract fun update(vararg roleDetails: RoleDetails)

    @Delete
    abstract fun delete(vararg ratings: Rating)
    @Delete
    abstract fun delete(vararg movies: Movie)
    @Delete
    abstract fun delete(vararg actors: Actor)
    @Delete
    abstract fun delete(vararg roles: Role)
//    @Delete
//    abstract fun delete(vararg roleDetails: RoleDetails)

    @Query("DELETE FROM Movie WHERE id in (:ids)")
    abstract fun deleteMoviesByIds(ids: List<String>)
    @Query("DELETE FROM Actor WHERE id in (:ids)")
    abstract fun deleteActorsByIds(ids: List<String>)
    @Query("DELETE FROM Rating WHERE id in (:ids)")
    abstract fun deleteRatingsByIds(ids: List<String>)

    @Query("DELETE FROM Movie")
    abstract fun clearMovies()
    @Query("DELETE FROM Actor")
    abstract fun clearActors()
    @Query("DELETE FROM Rating")
    abstract fun clearRatings()
    @Query("DELETE FROM Role")
    abstract fun clearRoles()




    @Transaction
    open fun resetDatabase() {
        clearMovies()
        clearActors()
        clearRoles()
        clearRatings()

        insert(NotRated, G, PG, PG13, R)

        insert(
            Movie("m1", "The Transporter", "Jason Statham kicks a guy in the face", "r3"),
            Movie("m2", "Transporter 2", "Jason Statham kicks a bunch of guys in the face", "r4"),
            Movie("m3", "Hobbs and Shaw", "Cars, Explosions and Stuff", "r3"),
            Movie("m4", "Jumanji - Welcome to the Jungle", "The Rock smolders", "r3"),
        )
        insert(
            Actor("a1", "Jason Statham"),
            Actor("a2", "The Rock"),
            Actor("a3", "Shu Qi"),
            Actor("a4", "Amber Valletta"),
            Actor("a5", "Kevin Hart"),
        )
//        insert(
//            RoleDetails("r1", "m1", "a1", "Frank Martin", 1),
//            RoleDetails("r2", "m1", "a3", "Lai", 2),
//            RoleDetails("r3", "m2", "a1", "Frank Martin", 1),
//            RoleDetails("r4", "m2", "a4", "Audrey Billings", 2),
//            RoleDetails("r5", "m3", "a2", "Hobbs", 1),
//            RoleDetails("r6", "m3", "a1", "Shaw", 2),
//            RoleDetails("r7", "m4", "a2", "Spencer", 1),
//            RoleDetails("r8", "m4", "a5", "Fridge", 2),
//        )
        insert(
            Role("m1", "a1", "r1"),
            Role("m1", "a3", "r2"),
            Role("m2", "a1", "r3"),
            Role("m2", "a4", "r4"),
            Role("m3", "a2", "r5"),
            Role("m3", "a1", "r6"),
            Role("m4", "a2", "r7"),
            Role("m4", "a5", "r8"),
        )
    }
}