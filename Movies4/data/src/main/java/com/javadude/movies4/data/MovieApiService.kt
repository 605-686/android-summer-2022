package com.javadude.movies4.data

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

const val BASE_URL = "http://10.0.2.2:8080" // host computer for emulator
    // NOTE: In a real app, you might allow the user to set this via settings

interface MovieApiService {
    @GET("movie")
    suspend fun getMovies(): Response<List<Movie>>

    @GET("actor")
    suspend fun getActors(): Response<List<Actor>>

    @GET("rating")
    suspend fun getRatings(): Response<List<Rating>>

    @GET("rating/{id}")
    suspend fun getRatingWithMovies(@Path("id") id: String): Response<RatingWithMovies>

    @GET("movie/{id}")
    suspend fun getMovieWithRoles(@Path("id") id: String): Response<MovieWithRoles>

    @GET("actor/{id}")
    suspend fun getActorWithRoles(@Path("id") id: String): Response<ActorWithRoles>

    @PUT("movie/{id}")
    suspend fun updateMovie(
        @Path("id") id: String,
        @Body movie: Movie
    ): Response<Int> // number updated

    @PUT("actor/{id}")
    suspend fun updateActor(
        @Path("id") id: String,
        @Body actor: Actor
    ): Response<Int> // number updated

    @PUT("actor/{id}")
    suspend fun updateRating(
        @Path("id") id: String,
        @Body rating: Rating
    ): Response<Int> // number updated

    @POST("movie")
    suspend fun createMovie(@Body movie: Movie): Response<Movie>

    @POST("actor")
    suspend fun createActor(@Body actor: Actor): Response<Actor>

    @POST("rating")
    suspend fun createRating(@Body rating: Rating): Response<Actor>

    @DELETE("movie/{id}")
    suspend fun deleteMovie(@Path("id") id: String): Response<Int> // number deleted

    @DELETE("actor/{id}")
    suspend fun deleteActor(@Path("id") id: String): Response<Int> // number deleted

    @DELETE("rating/{id}")
    suspend fun deleteRating(@Path("id") id: String): Response<Int> // number deleted

    @GET("reset")
    suspend fun resetDatabase(): Response<Int>

    companion object {
        fun create() =
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(MovieApiService::class.java)
    }
}

