package com.javadude.movies4.data

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 1,
    entities = [
        Movie::class,
        Actor::class,
        Role::class,
//        RoleDetails::class,
        Rating::class,
    ],
    exportSchema = false
)
abstract class MovieDatabase: RoomDatabase() {
    abstract fun dao(): MovieDAO
}