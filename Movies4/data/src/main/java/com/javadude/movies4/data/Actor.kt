package com.javadude.movies4.data

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Junction
import androidx.room.PrimaryKey
import androidx.room.Relation
import java.util.UUID

@Entity
data class Actor(
    @PrimaryKey var id: String = UUID.randomUUID().toString(),
    var name: String,
)

data class ActorWithRoles(
    @Embedded val actor: Actor,
    @Relation(
        parentColumn = "id",
        entityColumn = "id",
        associateBy = Junction(
            Role::class,
            parentColumn = "actorId",
            entityColumn = "movieId",
        ),
    )
    val roles: List<Movie>,
)