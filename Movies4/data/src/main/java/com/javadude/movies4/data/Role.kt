package com.javadude.movies4.data

import androidx.room.Entity
import androidx.room.Index
import androidx.room.ForeignKey

@Entity(
    primaryKeys = ["actorId", "movieId"],
    indices = [
        Index(value = ["movieId"]),
        Index(value = ["actorId"]),
        Index(value = ["roleDetailsId"]),
    ],
    foreignKeys = [
        ForeignKey(
            entity = Actor::class,
            parentColumns = ["id"],
            childColumns = ["actorId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        ),
        ForeignKey(
            entity = Movie::class,
            parentColumns = ["id"],
            childColumns = ["movieId"],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE,
        ),
    ]
)
data class Role(
    var movieId: String,
    var actorId: String,
    var roleDetailsId: String,
)

//@Entity(
//    foreignKeys = [
//        ForeignKey(
//            entity = Role::class,
//            parentColumns = ["roleDetailsId"],
//            childColumns = ["id"],
//            onUpdate = ForeignKey.CASCADE,
//            onDelete = ForeignKey.CASCADE,
//        ),
//    ]
//)
//class RoleDetails(
//    @PrimaryKey var id: String = UUID.randomUUID().toString(),
//    var movieId: String,
//    var actorId: String,
//    var character: String,
//    var orderInCredits: Int,
//)
