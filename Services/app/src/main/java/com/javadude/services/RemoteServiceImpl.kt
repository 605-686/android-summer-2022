package com.javadude.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicInteger

class RemoteServiceImpl: Service() {
    private val reporters = mutableListOf<RemoteServiceReporter>()
    private val scope = CoroutineScope(Dispatchers.IO)
    private var job: Job? = null

    val i = AtomicInteger(1)

    private val binder = object: RemoteService.Stub() {
        override fun reset() {
            i.set(1)
        }
        override fun add(reporter: RemoteServiceReporter) {
            reporters.add(reporter)
        }
        override fun remove(reporter: RemoteServiceReporter) {
            reporters.remove(reporter)
        }
    }
    override fun onBind(intent: Intent): IBinder {
        // if not already launched, launch a coroutine to start counting
        if (job == null) {
            job = scope.launch {
                while(true) {
                    Log.d("StartedService", "count = $i")
                    val iValue = i.get()
                    val people = listOf(
                        Person("Jenny", 10 + iValue),
                        Person("Tommy", 5 + iValue)
                    )
                    reporters.forEach {
                        it.report(people, iValue)
                    }
                    delay(250)
                    i.getAndIncrement()
                }
            }
        }
        return binder
    }

    override fun onDestroy() {
        job?.cancel()
        super.onDestroy()
    }
}