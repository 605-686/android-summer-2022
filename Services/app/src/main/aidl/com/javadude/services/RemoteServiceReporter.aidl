package com.javadude.services;
import com.javadude.services.Person;

// An example callback interface that sends a list of people and a number back to the requester
interface RemoteServiceReporter {
	void report(in List<Person> people, in int n);
}