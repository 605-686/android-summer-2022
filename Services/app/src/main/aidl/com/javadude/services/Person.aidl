package com.javadude.services;

// AIDL that declares (to the AIDL compiler) that we have a class named
//    com.javadude.services.Person that's Parcelable
parcelable Person;