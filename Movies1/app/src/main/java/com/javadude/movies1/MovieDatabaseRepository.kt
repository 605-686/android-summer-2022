package com.javadude.movies1

import android.app.Application
import androidx.room.Room
import com.javadude.movies1.data.Actor
import com.javadude.movies1.data.ActorWithRoles
import com.javadude.movies1.data.Movie
import com.javadude.movies1.data.MovieDatabase
import com.javadude.movies1.data.MovieWithRoles
import com.javadude.movies1.data.Rating
import com.javadude.movies1.data.RatingWithMovies
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieDatabaseRepository(private val application: Application): MovieRepository {
    private val db =
        Room.databaseBuilder(
            application,
            MovieDatabase::class.java,
            "MOVIES"
        ).build()

    override val ratingsFlow = db.dao().getRatingsFlow()
    override val moviesFlow = db.dao().getMoviesFlow()
    override val actorsFlow = db.dao().getActorsFlow()

    override suspend fun expand(rating: Rating): RatingWithMovies = withContext(Dispatchers.IO) {
        db.dao().getRatingWithMovies(rating.id)
    }
    override suspend fun expand(movie: Movie): MovieWithRoles = withContext(Dispatchers.IO) {
        db.dao().getMovieWithRoles(movie.id)
    }
    override suspend fun expand(actor: Actor): ActorWithRoles = withContext(Dispatchers.IO) {
        db.dao().getActorWithRoles(actor.id)
    }

    override suspend fun update(movie: Movie) = withContext(Dispatchers.IO) {
        db.dao().update(movie)
    }

    override suspend fun resetDatabase() = withContext(Dispatchers.IO) {
        db.dao().resetDatabase()
    }
}