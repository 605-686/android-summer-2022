package com.javadude.movies1.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import com.javadude.movies1.ActorsScreen
import com.javadude.movies1.MoviesScreen
import com.javadude.movies1.R
import com.javadude.movies1.RatingsScreen
import com.javadude.movies1.Screen
import com.javadude.movies1.SimpleButton
import com.javadude.movies1.emptyImmutableList
import com.javadude.movies1.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun MainScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    push: (Screen) -> Unit,
    onReset: () -> Unit,
    onScreenSelect: (Screen) -> Unit,
) {
    MovieScaffold(
        scope = scope,
        title = stringResource(id = R.string.screen_title_main),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            SimpleButton(text = "Ratings") {
                push(RatingsScreen)
            }
            SimpleButton(text = "Movies") {
                push(MoviesScreen)
            }
            SimpleButton(text = "Actors") {
                push(ActorsScreen)
            }
            SimpleButton(text = "Reset DB") {
                onReset()
            }
        }
    }
}

