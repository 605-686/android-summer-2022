package com.javadude.movies1

import com.javadude.movies1.data.Actor
import com.javadude.movies1.data.ActorWithRoles
import com.javadude.movies1.data.Movie
import com.javadude.movies1.data.MovieWithRoles
import com.javadude.movies1.data.Rating
import com.javadude.movies1.data.RatingWithMovies
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val ratingsFlow: Flow<List<Rating>>
    val moviesFlow: Flow<List<Movie>>
    val actorsFlow: Flow<List<Actor>>

    suspend fun expand(rating: Rating): RatingWithMovies
    suspend fun expand(movie: Movie): MovieWithRoles
    suspend fun expand(actor: Actor): ActorWithRoles

    suspend fun update(movie: Movie)

    suspend fun resetDatabase()
}