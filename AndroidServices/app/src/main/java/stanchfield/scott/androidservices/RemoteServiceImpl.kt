package stanchfield.scott.androidservices

import android.app.Service
import android.content.Intent
import android.os.IBinder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class RemoteServiceImpl: Service() {
    private val scope = CoroutineScope(Dispatchers.IO)
    private var job: Job? = null
    private val reporters = mutableListOf<RemoteServiceReporter>()
    private var n = 1

    private val binder = object: RemoteService.Stub() {
        override fun reset() {
            n = 1
        }
        override fun add(reporter: RemoteServiceReporter) {
            reporters.add(reporter)
            if (job == null) {
                job = scope.launch {
                    while(true) {
                        delay(250)
                        n++
                        val people = listOf(
                            Person("Scott", 10 + n),
                            Person("Trevor", 5 + n)
                        )
                        reporters.forEach {
                            it.report(people, n)
                        }
                    }
                }
            }
        }
        override fun remove(reporter: RemoteServiceReporter) {
            reporters.remove(reporter)
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    override fun onDestroy() {
        job?.cancel()
        job = null
        super.onDestroy()
    }
}