package com.javadude.toastsnackbardialog

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.javadude.toastsnackbardialog.ui.theme.ToastSnackbarDialogTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ToastSnackbarDialogTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Ui()
                }
            }
        }
    }
}

@Composable
fun MyButton(
    text: String,
    onClick: () -> Unit
) =
    Button(onClick = onClick) {
        Text(text = text)
    }

@Composable
fun Ui(
    viewModel: PersonViewModel = viewModel()
) {
    val people by viewModel.people.collectAsState(initial = emptyList())

    var personIdToDelete by rememberSaveable(true) {
        mutableStateOf<String?>(null)
    }
    var personNameToDelete by rememberSaveable(true) {
        mutableStateOf<String?>(null)
    }

    personIdToDelete?.let { personId ->
        AlertDialog(
            onDismissRequest = { personIdToDelete = null },
            title = { Text(text = "Delete Person?")},
            text = {
                personNameToDelete?.let {
                    Text(text = "Do you really want to delete $it?")
                }
            },
            confirmButton = {
                MyButton(text = "Delete") {
                    viewModel.deletePersonAsync(personId)
                    personIdToDelete = null
                    personNameToDelete = null
                }
            },
            dismissButton = {
                MyButton(text = "Cancel") {
                    personIdToDelete = null
                    personNameToDelete = null
                }
            },
        )
    }


    PeopleUi(
        people = people,
        onDelete = {
            personNameToDelete = it.name
            personIdToDelete = it.id
        }
    )
}

@Composable
fun PeopleUi(
    people: List<Person>,
    onDelete: (Person) -> Unit
) {
    Column {
        people.forEach { person ->
            Row {
               Text(
                   text = person.name,
                   modifier = Modifier
                       .padding(8.dp)
                       .weight(1f),
               )
               Icon(
                   imageVector = Icons.Default.Delete,
                   contentDescription = "Delete Person",
                   modifier = Modifier
                       .padding(8.dp)
                       .clickable { onDelete(person) }
               )
            }
        }
    }
}
