package com.javadude.toastsnackbardialog

interface Command {
    fun execute()
    fun undo()
    fun redo()
}