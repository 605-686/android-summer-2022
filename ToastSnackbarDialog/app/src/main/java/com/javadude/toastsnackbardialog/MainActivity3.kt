package com.javadude.toastsnackbardialog

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Redo
import androidx.compose.material.icons.filled.Undo
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import com.javadude.toastsnackbardialog.ui.theme.ToastSnackbarDialogTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity3 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ToastSnackbarDialogTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Ui3()
                }
            }
        }
    }
}

@Composable
fun Ui3(
    viewModel: PersonViewModel = viewModel()
) {
    val people by viewModel.people.collectAsState(initial = emptyList())

    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()
    val context = LocalContext.current

    var currentToast: Toast? = null

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(
                title = { Text(text = "Sample Application") },
                actions = {
                    ActionButton(
                        enabled = viewModel.undoAvailable,
                        imageVector = Icons.Default.Undo,
                        contentDescription = "Undo"
                    ) {
                        viewModel.undo()
                    }
                    ActionButton(
                        enabled = viewModel.redoAvailable,
                        imageVector = Icons.Default.Redo,
                        contentDescription = "Redo"
                    ) {
                        viewModel.redo()
                    }
                }
            )
        },
        content = {
            PeopleUi(
                people = people,
                onDelete = { person ->
                    scope.launch(Dispatchers.Default) {
                        scaffoldState.snackbarHostState.currentSnackbarData?.dismiss()
                        viewModel.deletePersonAsyncWithUndo(person)
                        withContext(Dispatchers.Main) {
                            currentToast?.cancel()
                            currentToast = Toast.makeText(context, "${person.name} deleted", Toast.LENGTH_LONG).apply {
                                show()
                            }
                        }
                    }
                }
            )
        }
    )
}

@Composable
fun ActionButton(
    enabled: Boolean,
    imageVector: ImageVector,
    contentDescription: String,
    onClick: () -> Unit
) =
    IconButton(enabled = enabled, onClick = onClick) {
        Icon(imageVector = imageVector, contentDescription = contentDescription)
    }
