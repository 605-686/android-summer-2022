package com.javadude.toastsnackbardialog

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

data class Person(
    val id: String,
    val name: String,
    val age: Int,
)

class PersonViewModel: ViewModel() {
    inner class DeletePersonCommand(
        private val person: Person
    ): Command {
        override fun execute() {
            deletePersonAsync(person.id)
        }
        override fun undo() {
            addPerson(person.id, person.name, person.age)
        }
        override fun redo() {
            execute()
        }
    }

    private val undoManager = UndoManager()

    val undoAvailable: Boolean
        get() = undoManager.undoAvailable
    val redoAvailable: Boolean
        get() = undoManager.redoAvailable
    fun undo() = undoManager.undo()
    fun redo() = undoManager.redo()

    private val peopleMap = MutableStateFlow(
        mapOf(
            "p1" to Person("p1", "Scott", 55),
            "p2" to Person("p2", "Mike", 5),
            "p3" to Person("p3", "Sue", 25),
            "p4" to Person("p4", "Martha", 30),
        )
    )
    val people = peopleMap.map { map ->
        map.values.sortedBy { it.name }
    }
    fun deletePersonAsyncWithUndo(person: Person) {
        viewModelScope.launch(Dispatchers.IO) {
            undoManager.execute(DeletePersonCommand(person))
        }
    }
    fun deletePersonAsync(personId: String) {
        viewModelScope.launch(Dispatchers.IO) {
            peopleMap.value = peopleMap.value - personId
        }
    }
    suspend fun deletePerson(personId: String) = withContext(Dispatchers.IO) {
        peopleMap.value = peopleMap.value - personId
    }
    private fun add(person: Person) {
        peopleMap.value = peopleMap.value + (person.id to person)
    }
    fun addPerson(id: String, name: String, age: Int) {
        add(Person(id, name, age))
    }
}