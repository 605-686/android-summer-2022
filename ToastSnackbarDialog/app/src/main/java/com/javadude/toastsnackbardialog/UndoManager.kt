package com.javadude.toastsnackbardialog

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue

class UndoManager {
    var undoAvailable by mutableStateOf(false)
    var redoAvailable by mutableStateOf(false)

    private var undoStack = emptyList<Command>()
        set(value) {
            field = value
            undoAvailable = value.isNotEmpty()
        }
    private var redoStack = emptyList<Command>()
        set(value) {
            field = value
            redoAvailable = value.isNotEmpty()
        }
    private val lock = Any()

    fun execute(command: Command) {
        command.execute()
        undoStack = undoStack + command
        redoStack = emptyList()
    }
    fun undo() {
        synchronized(lock) {
            undoStack.lastOrNull()?.let { toUndo ->
                undoStack = undoStack.dropLast(1)
                toUndo.undo()
                redoStack = redoStack + toUndo
            }
        }
    }
    fun redo() {
        synchronized(lock) {
            redoStack.lastOrNull()?.let { toRedo ->
                redoStack = redoStack.dropLast(1)
                toRedo.redo()
                undoStack = undoStack + toRedo
            }
        }
    }
}