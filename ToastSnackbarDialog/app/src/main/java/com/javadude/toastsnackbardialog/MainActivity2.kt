package com.javadude.toastsnackbardialog

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarResult
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import com.javadude.toastsnackbardialog.ui.theme.ToastSnackbarDialogTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ToastSnackbarDialogTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    Ui2()
                }
            }
        }
    }
}

@Composable
fun Ui2(
    viewModel: PersonViewModel = viewModel()
) {
    val people by viewModel.people.collectAsState(initial = emptyList())

    val scaffoldState = rememberScaffoldState()
    val scope = rememberCoroutineScope()
    val context = LocalContext.current

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            TopAppBar(title = { Text(text = "Sample Application") })
        },
        content = {
            PeopleUi(
                people = people,
                onDelete = { person ->
                    scope.launch(Dispatchers.Default) {
                        scaffoldState.snackbarHostState.currentSnackbarData?.dismiss()
                        viewModel.deletePerson(person.id)
                        val result = scaffoldState.snackbarHostState.showSnackbar(
                            message = "${person.name} deleted",
                            actionLabel = "Undo",
                        )
                        when (result) {
                            SnackbarResult.Dismissed -> { } // do nothing
                            SnackbarResult.ActionPerformed -> {
                                viewModel.addPerson(person.id, person.name, person.age)
                                withContext(Dispatchers.Main) {
                                    Toast.makeText(context, "${person.name} restored", Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                    }
                }
            )
        }
    )
}
