package stanchfield.scott.kotlinprimer

class Person5 {
    // backing field exists if EITHER get/set references "field"
    var name1 = ""
        get() {
            return field // return BACKING FIELD
        }
        set(value) {
            // log that the value changed
            field = value // change BACKING FIELD
            doctorName1 = "Dr. $value"
        }

    var doctorName1: String = ""
        private set

    var name2 = ""
    val doctorName2: String  // DERIVED PROPERTY
        get() = "Dr. $name2"

    var age = 0
}

interface Animal
data class Cat(var name: String): Animal {
    fun meow() {
        println("meow")
    }
}
data class Dog(var name: String): Animal {
    fun bark() {
        println("ruff")
    }
}


var x = 42
var animal: Animal? = null

fun main() {
    val list1 = listOf(1, 2, 3, 4, 5)

    for (n in list1) {
        println(n)
    }
    for (n in 1..5) {
        println(n)
    }
    for (n in 1 until 5) {
        println(n)
    }
    for (n in 1 until 5 step 2) {
        println(n)
    }

    when (x) {
        1 -> { println("a") }
        2 -> println("a")
        42 -> {}
        else -> {}
    }

    val y = when (x) {
        1 -> 10
        2 -> 20
        42 -> 100
        else -> 1
    }

    animal = Cat("Floooooofy")

    // some point later
    when (val current = animal) {
        is Cat -> current.meow()
        is Dog -> current.bark()
        else -> {}
    }


    when {
        y == 10 -> {}
        animal is Cat -> {}
        else -> {}
    }
}

