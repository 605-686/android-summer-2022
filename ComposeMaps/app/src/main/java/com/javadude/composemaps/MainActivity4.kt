@file:OptIn(ExperimentalMaterialApi::class)

package com.javadude.composemaps

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.material.ExposedDropdownMenuDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.platform.LocalContext
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.Priority
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.CameraPositionState
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.MarkerInfoWindowContent
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.rememberCameraPositionState
import com.javadude.composemaps.ui.theme.ComposeMapsTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext


private val defaultCameraPosition = CameraPosition.fromLatLngZoom(LatLng(37.42563584665582, -122.0804072224463), 18f)

class MainActivity4 : ComponentActivity() {
    private val viewModel: CarViewModel by viewModels()
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient

    private val locationCallback = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            viewModel.updateLocation(locationResult.lastLocation)
        }
    }

    private val getLocationPermission = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGranted ->
        if (isGranted.values.any { it }) {
            startLocationAndMap()
        } else {
            AlertDialog.Builder(this)
                .setTitle("Permissions Needed")
                .setMessage("We need coarse-location or fine-location permission to locate a car (fine location is highly recommended for accurate car locating). Please allow these permissions via App Info settings")
                .setCancelable(false)
                .setNegativeButton("Quit") { _,_ -> finish() }
                .setPositiveButton("App Info") { _,_ ->
                    startActivity(
                        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
                            data = Uri.parse("package:$packageName")
                        }
                    )
                    finish()
                }
                .show()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        GoogleApiAvailability.getInstance().makeGooglePlayServicesAvailable(this)
            .addOnSuccessListener {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {
                    getLocationPermission.launch(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION))
                } else {
                    startLocationAndMap()
                }
            }.addOnFailureListener(this) {
                Toast.makeText(this, "Google Play services required (or upgrade required)", Toast.LENGTH_SHORT).show()
                finish()
            }
    }

    @SuppressLint("MissingPermission")
    fun startLocationAndMap() {
        val locationRequest = LocationRequest.create().apply {
            priority = Priority.PRIORITY_HIGH_ACCURACY
            interval = 5
            fastestInterval = 0
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )

        setContent {
            ComposeMapsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var mapLoaded by remember { mutableStateOf(false) }

                    val cameraPositionState = rememberCameraPositionState {
                        position = defaultCameraPosition
                    }

                    val currentLocation by viewModel.currentLocation.collectAsState(initial = null)
                    val carLatLng by viewModel.carLatLng.collectAsState(initial = null)

                    Scaffold(
                        topBar = {
                            CarTopBar(
                                currentLocation = currentLocation,
                                carLatLng = carLatLng,
                                onSetCarLocation = viewModel::setCarLocation,
                                onClearCarLocation = viewModel::clearCarLocation,
                                onWalkToCar = {
                                    currentLocation?.let { curr ->
                                        carLatLng?.let { car ->
                                            val uri = Uri.parse("https://www.google.com/maps/dir/?api=1&origin=${curr.latitude},${curr.longitude}&destination=${car.latitude},${car.longitude}&travelmode=walking")
                                            startActivity(Intent(Intent.ACTION_VIEW, uri).apply {
                                                setPackage("com.google.android.apps.maps")
                                            })
                                        } ?: Toast.makeText(this, "Cannot navigate; no car location available", Toast.LENGTH_LONG).show()
                                    } ?: Toast.makeText(this, "Cannot navigate; no current location available", Toast.LENGTH_LONG).show()
                                },
                            )
                        },
                        content = {
                            Box(modifier = Modifier.fillMaxSize().padding(it)) {
                                GoogleMapView4(
                                    cameraPositionState = cameraPositionState,
                                    onMapLoaded = { mapLoaded = true },
                                    car = carLatLng,
                                    modifier = Modifier.fillMaxSize()
                                )
                                if (!mapLoaded) {
                                    AnimatedVisibility(
                                        visible = !mapLoaded,
                                        modifier = Modifier.fillMaxSize(),
                                        enter = EnterTransition.None,
                                        exit = fadeOut()
                                    ) {
                                        CircularProgressIndicator(
                                            modifier = Modifier
                                                .background(MaterialTheme.colors.background)
                                                .wrapContentSize()
                                        )
                                    }
                                }
                            }
                        }
                    )
                }
            }
        }
    }
}


@Composable
fun GoogleMapView4(
    cameraPositionState: CameraPositionState,
    onMapLoaded: () -> Unit,
    car: LatLng?,
    modifier: Modifier,
) {
    var carIcon by remember { mutableStateOf<BitmapDescriptor?>(null) }
    val context = LocalContext.current

    LaunchedEffect(true) {
        withContext(Dispatchers.IO) {
            carIcon = context.loadBitmapDescriptor(R.drawable.ic_car)
        }
    }

    val carState = remember(car) { car?.let { MarkerState(it) } }

    val uiSettings = remember {
        MapUiSettings(
            compassEnabled = false,
            rotationGesturesEnabled = false,
        )
    }

    var mapProperties by remember {
        mutableStateOf(MapProperties(mapType = MapType.NORMAL))
    }

    var currentMapType by remember { mutableStateOf(MapType.NORMAL) }

    LaunchedEffect(Unit) {
        delay(3000)
        onMapLoaded()
    }

    Column(modifier = modifier) {
        MapTypeControls4(
            currentValue = currentMapType,
            modifier = Modifier.fillMaxWidth(),
        ) {
            Log.d("GoogleMap", "Selected map type $it")
            mapProperties = mapProperties.copy(mapType = it)
            currentMapType = it
        }
        GoogleMap(
            cameraPositionState = cameraPositionState,
            uiSettings = uiSettings,
            properties = mapProperties,
            onMapLoaded = onMapLoaded,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
        ) {
            carState?.let {
                MarkerInfoWindowContent(
                    state = it,
                    icon = carIcon,
                    anchor = Offset(0.5f, 0.5f),
                    title = "Car Location", // should be in strings.xml
                    draggable = true
                )
            }
        }
    }
}

@Composable
private fun MapTypeControls4(
    currentValue: MapType,
    modifier: Modifier,
    onMapTypeClick: (MapType) -> Unit,
) {
    var expanded by remember {
        mutableStateOf(false)
    }

    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = { expanded = !expanded },
        modifier = modifier,
    ) {
        TextField(
            value = currentValue.name,
            label = {
                Text(text = "Map Type")
            },
            readOnly = true,
            onValueChange = {},
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
            },
            colors = ExposedDropdownMenuDefaults.textFieldColors(),
            modifier = Modifier.fillMaxWidth()
        )
        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth(),
        ) {
            MapType.values().forEach {
                DropdownMenuItem(onClick = {
                    onMapTypeClick(it)
                    expanded = false
                }) {
                    Text(text = it.name)
                }
            }
        }
    }
}

@Composable
private fun CarTopBar(
    currentLocation: Location?,
    carLatLng: LatLng?,
    onSetCarLocation: () -> Unit,
    onClearCarLocation: () -> Unit,
    onWalkToCar: () -> Unit,
) {
    TopAppBar(
        title = { Text(text = "Car Finder")},
        actions = {
            currentLocation?.let {
                IconButton(onClick = onSetCarLocation) {
                    Icon(imageVector = Icons.Filled.Star, contentDescription = "Remember Location")
                }
            }
            carLatLng?.let {
                IconButton(onClick = onWalkToCar) {
                    Icon(imageVector = Icons.Filled.DirectionsWalk, contentDescription = "Remember Car Location")
                }
                IconButton(onClick = onClearCarLocation) {
                    Icon(imageVector = Icons.Filled.Delete, contentDescription = "Forget Car Location")
                }
            }
        },
    )
}