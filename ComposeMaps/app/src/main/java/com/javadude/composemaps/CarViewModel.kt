package com.javadude.composemaps

import android.app.Application
import android.location.Location
import androidx.core.content.edit
import androidx.lifecycle.AndroidViewModel
import androidx.preference.PreferenceManager
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow

private const val LAT_PREF = "lat"
private const val LON_PREF = "lon"

class CarViewModel(application: Application) : AndroidViewModel(application) {
    private val _currentLocation = MutableStateFlow<Location?>(null)
    val currentLocation: Flow<Location?>
        get() = _currentLocation

    fun updateLocation(location: Location?) {
        _currentLocation.value = location
    }

    private val _carLatLng = MutableStateFlow(
        PreferenceManager.getDefaultSharedPreferences(getApplication()).let {
            it.getString(LAT_PREF, null)?.let { latString ->
                it.getString(LON_PREF, null)?.let { lonString ->
                    LatLng(latString.toDouble(), lonString.toDouble())
                }
            }
        }
    )
    val carLatLng: Flow<LatLng?>
        get() = _carLatLng

    fun clearCarLocation() {
        PreferenceManager.getDefaultSharedPreferences(getApplication()).edit {
            remove(LAT_PREF)
            remove(LON_PREF)
        }
        _carLatLng.value = null
    }
    fun setCarLocation(): LatLng? {
        val newLatLng = _currentLocation.value?.let {
            PreferenceManager.getDefaultSharedPreferences(getApplication()).edit {
                putString(LAT_PREF, it.latitude.toString())
                putString(LON_PREF, it.longitude.toString())
            }
            LatLng(it.latitude, it.longitude)
        } ?: run {
            clearCarLocation()
            null
        }
        _carLatLng.value = newLatLng
        return newLatLng
    }
}