@file:OptIn(ExperimentalMaterialApi::class)

package com.javadude.composemaps

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.material.ExposedDropdownMenuDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.platform.LocalContext
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.maps.android.compose.CameraPositionState
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.MarkerInfoWindowContent
import com.google.maps.android.compose.MarkerState
import com.google.maps.android.compose.Polyline
import com.google.maps.android.compose.rememberCameraPositionState
import com.javadude.composemaps.ui.theme.ComposeMapsTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


private val cars = listOf(
    LatLng(37.42563584665582, -122.0804072224463),
    LatLng(37.4248334039287, -122.08044330980334),
    LatLng(37.42788550630341, -122.07872916034479),
)
private val defaultCameraPosition = CameraPosition.fromLatLngZoom(cars[0], 18f)

class MainActivity3 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeMapsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var mapLoaded by remember { mutableStateOf(false) }

                    val cameraPositionState = rememberCameraPositionState {
                        position = defaultCameraPosition
                    }

                    var carsToDisplay by remember {
                        mutableStateOf(emptyList<LatLng>())
                    }

                    LaunchedEffect(cars) {
                        carsToDisplay = emptyList()
                        withContext(Dispatchers.Default) {
                            cars.forEach { car ->
                                delay(3000)
                                carsToDisplay = carsToDisplay + car
                            }
                        }
                    }

                    val scope = rememberCoroutineScope()
                    LaunchedEffect(carsToDisplay) {
                        if (carsToDisplay.isNotEmpty()) {
                            var bounds: LatLngBounds? = null
                            carsToDisplay.forEach { car ->
                                bounds = bounds?.including(car) ?: LatLngBounds(car, car)
                            }
                            bounds?.let { bounds ->
                                scope.launch {
                                    cameraPositionState.animate(CameraUpdateFactory.newLatLngBounds(bounds, 100), 1000)
                                }
                            }
                        }
                    }

                    Box(modifier = Modifier.fillMaxSize()) {
                        GoogleMapView3(
                            cameraPositionState = cameraPositionState,
                            onMapLoaded = { mapLoaded = true },
                            cars = carsToDisplay,
                            modifier = Modifier.fillMaxSize()
                        )
                        if (!mapLoaded) {
                            AnimatedVisibility(
                                visible = !mapLoaded,
                                modifier = Modifier.fillMaxSize(),
                                enter = EnterTransition.None,
                                exit = fadeOut()
                            ) {
                                CircularProgressIndicator(
                                    modifier = Modifier
                                        .background(MaterialTheme.colors.background)
                                        .wrapContentSize()
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}


@Composable
fun GoogleMapView3(
    cameraPositionState: CameraPositionState,
    onMapLoaded: () -> Unit,
    cars: List<LatLng>,
    modifier: Modifier,
) {
    var carIcon by remember { mutableStateOf<BitmapDescriptor?>(null) }
    val context = LocalContext.current

    LaunchedEffect(true) {
        withContext(Dispatchers.IO) {
            carIcon = context.loadBitmapDescriptor(R.drawable.ic_car)
        }
    }

    val carStates = remember(cars) {
        cars.map { MarkerState(it) }
    }

    val uiSettings = remember {
        MapUiSettings(
            compassEnabled = false,
            rotationGesturesEnabled = false,
        )
    }

    var mapProperties by remember {
        mutableStateOf(MapProperties(mapType = MapType.NORMAL))
    }

    var currentMapType by remember { mutableStateOf(MapType.NORMAL) }

    LaunchedEffect(Unit) {
        delay(3000)
        onMapLoaded()
    }

    Column(modifier = modifier) {
        MapTypeControls3(
            currentValue = currentMapType,
            modifier = Modifier.fillMaxWidth(),
        ) {
            Log.d("GoogleMap", "Selected map type $it")
            mapProperties = mapProperties.copy(mapType = it)
            currentMapType = it
        }
        GoogleMap(
            cameraPositionState = cameraPositionState,
            uiSettings = uiSettings,
            properties = mapProperties,
            onMapLoaded = onMapLoaded,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
        ) {
            carStates.forEachIndexed { n, carState ->
                MarkerInfoWindowContent(
                    state = carState,
                    icon = carIcon,
                    anchor = Offset(0.5f, 0.5f),
//                    onClick = {
//                        Log.d("Maps1", "Clicked at ${it.position}")
//                        cameraPositionState.projection?.let {
//                            Log.d("Maps1", "Screen bounds $it")
//                        }
//                        true
//                    },
                    title = "Car $n", // should be in strings.xml
                    draggable = true
                )
            }
            Polyline(
                points = carStates.map { it.position }
            )
        }
    }
}

@Composable
private fun MapTypeControls3(
    currentValue: MapType,
    modifier: Modifier,
    onMapTypeClick: (MapType) -> Unit,
) {
    var expanded by remember {
        mutableStateOf(false)
    }

    ExposedDropdownMenuBox(
        expanded = expanded,
        onExpandedChange = { expanded = !expanded },
        modifier = modifier,
    ) {
        TextField(
            value = currentValue.name,
            label = {
                Text(text = "Map Type")
            },
            readOnly = true,
            onValueChange = {},
            trailingIcon = {
                ExposedDropdownMenuDefaults.TrailingIcon(expanded = expanded)
            },
            colors = ExposedDropdownMenuDefaults.textFieldColors(),
            modifier = Modifier.fillMaxWidth()
        )
        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.fillMaxWidth(),
        ) {
            MapType.values().forEach {
                DropdownMenuItem(onClick = {
                    onMapTypeClick(it)
                    expanded = false
                }) {
                    Text(text = it.name)
                }
            }
        }
    }
}