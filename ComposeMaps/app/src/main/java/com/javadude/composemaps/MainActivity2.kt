package com.javadude.composemaps

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.ExposedDropdownMenuBox
import androidx.compose.material.ExposedDropdownMenuDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.platform.LocalContext
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.compose.CameraPositionState
import com.google.maps.android.compose.Circle
import com.google.maps.android.compose.DragState
import com.google.maps.android.compose.GoogleMap
import com.google.maps.android.compose.MapProperties
import com.google.maps.android.compose.MapType
import com.google.maps.android.compose.MapUiSettings
import com.google.maps.android.compose.MarkerInfoWindowContent
import com.google.maps.android.compose.rememberCameraPositionState
import com.google.maps.android.compose.rememberMarkerState
import com.javadude.composemaps.ui.theme.ComposeMapsTheme
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext

private val googleHQ = LatLng(37.42423291057923, -122.08811454627153)
private val defaultCameraPosition = CameraPosition.fromLatLngZoom(googleHQ, 11f)

class MainActivity2 : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeMapsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    var mapLoaded by remember { mutableStateOf(false) }

                    val cameraPositionState = rememberCameraPositionState {
                        position = defaultCameraPosition
                    }

                    Box(modifier = Modifier.fillMaxSize()) {
                        GoogleMapView2(
                            cameraPositionState = cameraPositionState,
                            onMapLoaded = { mapLoaded = true },
                            place = googleHQ,
                            modifier = Modifier.fillMaxSize()
                        )
                        if (!mapLoaded) {
                            AnimatedVisibility(
                                visible = !mapLoaded,
                                modifier = Modifier.fillMaxSize(),
                                enter = EnterTransition.None,
                                exit = fadeOut()
                            ) {
                                CircularProgressIndicator(
                                    modifier = Modifier
                                        .background(MaterialTheme.colors.background)
                                        .wrapContentSize()
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}


@Composable
fun GoogleMapView2(
    cameraPositionState: CameraPositionState,
    onMapLoaded: () -> Unit,
    place: LatLng,
    modifier: Modifier,
) {
    var carIcon by remember { mutableStateOf<BitmapDescriptor?>(null) }
    val context = LocalContext.current

    LaunchedEffect(true) {
        withContext(Dispatchers.IO) {
            carIcon = context.loadBitmapDescriptor(R.drawable.ic_car)
        }
    }

    val placeState = rememberMarkerState(position = place)
    val uiSettings = remember {
        MapUiSettings(
            compassEnabled = false,
            rotationGesturesEnabled = false,
        )
    }

    val mapProperties by remember {
        mutableStateOf(MapProperties(mapType = MapType.NORMAL))
    }

    LaunchedEffect(Unit) {
        delay(3000)
        onMapLoaded()
    }

    var circleCenter by remember { mutableStateOf(googleHQ) }
    if (placeState.dragState == DragState.END) {
        circleCenter = placeState.position
    }


    Column(modifier = modifier) {
        GoogleMap(
            cameraPositionState = cameraPositionState,
            uiSettings = uiSettings,
            properties = mapProperties,
            onMapLoaded = onMapLoaded,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
        ) {
            MarkerInfoWindowContent(
                state = placeState,
                icon = carIcon,
                anchor = Offset(0.5f, 0.5f),
                onClick = {
                    Log.d("Maps1", "Clicked at ${it.position}")
                    cameraPositionState.projection?.let {
                        Log.d("Maps1", "Screen bounds $it")
                    }
                    true
                },
                title = "Sample Description", // should be in strings.xml
                draggable = true
            )
            Circle(
                center = circleCenter,
                fillColor = MaterialTheme.colors.secondary,
                strokeColor = MaterialTheme.colors.secondaryVariant,
                radius = 3000.0,
            )
        }
    }

}
