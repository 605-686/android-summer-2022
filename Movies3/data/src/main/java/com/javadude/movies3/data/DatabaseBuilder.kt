package com.javadude.movies3.data

import android.app.Application
import androidx.room.Room

fun createDao(application: Application) =
    Room.databaseBuilder(
        application,
        MovieDatabase::class.java,
        "MOVIES"
    ).build().dao()