package com.javadude.movies3.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies3.Display
import com.javadude.movies3.Label
import com.javadude.movies3.R
import com.javadude.movies3.Screen
import com.javadude.movies3.repo.ActorDto
import com.javadude.movies3.repo.MovieDto
import com.javadude.movies3.repo.MovieWithRolesDto
import com.javadude.movies3.repo.immutableListOf
import com.javadude.movies3.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun MovieScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    movieWithRoles: MovieWithRolesDto?,
    select: (ActorDto) -> Unit,
    onEdit: (MovieDto) -> Unit,
) {
    requireNotNull(movieWithRoles)

    MovieScaffold(
        scope = scope,
        title = movieWithRoles.movie.title,
        topActions = immutableListOf(
            TopAction(
                icon = Icons.Default.Edit,
                contentDescriptionId = R.string.action_description_edit_movie,
            ) {
                onEdit(movieWithRoles.movie)
            }
        ),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            Label(labelId = R.string.label_description)
            Display(text = movieWithRoles.movie.title)

            Label(label = stringResource(id = R.string.label_cast_of_movie, movieWithRoles.movie.title))

            movieWithRoles.roles
                .forEach {
                    Display(text = it.name, modifier = Modifier.clickable { select(it) })
                }
        }
    }
}