package com.javadude.movies3.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies3.Display
import com.javadude.movies3.Label
import com.javadude.movies3.R
import com.javadude.movies3.Screen
import com.javadude.movies3.repo.MovieDto
import com.javadude.movies3.repo.RatingWithMoviesDto
import com.javadude.movies3.repo.emptyImmutableList
import com.javadude.movies3.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun RatingScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    ratingWithMovies: RatingWithMoviesDto?,
    select: (MovieDto) -> Unit,
) {
    MovieScaffold(
        scope = scope,
        title = ratingWithMovies?.rating?.name ?: stringResource(id = R.string.screen_title_rating),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            Label(labelId = R.string.label_description)
            Display(text = ratingWithMovies?.rating?.description ?: "")

            Label(
                label = stringResource(
                    id = R.string.label_movies_with_rating,
                    ratingWithMovies?.rating?.name ?: ""
                )
            )
            ratingWithMovies?.movies?.forEach {
                Display(text = it.title, modifier = Modifier.clickable { select(it) })
            }
        }
    }
}

