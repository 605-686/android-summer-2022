package com.javadude.movies3.screens

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.javadude.movies3.R
import com.javadude.movies3.Screen
import com.javadude.movies3.TextField
import com.javadude.movies3.repo.MovieDto
import com.javadude.movies3.repo.MovieWithRolesDto
import com.javadude.movies3.repo.emptyImmutableList
import com.javadude.movies3.screenTargets
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

@Composable
fun MovieEditScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    movieWithRoles: MovieWithRolesDto?,
    onMovieChange: suspend (MovieDto) -> Unit,
) {
    requireNotNull(movieWithRoles)

    var title by remember {
        mutableStateOf(movieWithRoles.movie.title)
    }
    var description by remember {
        mutableStateOf(movieWithRoles.movie.description)
    }

    MovieScaffold(
        scope = scope,
        title = title,
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column {
            TextField(
                labelId = R.string.label_title,
                placeholderId = R.string.placeholder_movie_title,
                value = title,
                onValueChange = {
                    title = it
                    scope.launch {
                        onMovieChange(movieWithRoles.movie.copy(title = it))
                    }
                }
            )
            TextField(
                labelId = R.string.label_description,
                placeholderId = R.string.placeholder_movie_description,
                value = description,
                onValueChange = {
                    description = it
                    scope.launch {
                        onMovieChange(movieWithRoles.movie.copy(description = it))
                    }
                }
            )
        }
    }
}