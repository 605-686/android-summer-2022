package com.javadude.movies3

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Immutable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp

@Immutable
data class ImmutableSet<T>(val set: Set<T>): Set<T> by set {
    operator fun plus(item: T) = ImmutableSet(set + item)
    operator fun minus(item: T) = ImmutableSet(set - item)
}

fun <T> immutableSetOf(vararg items: T) =
    ImmutableSet(setOf(*items))

fun <T> emptyImmutableSet() = ImmutableSet<T>(emptySet())


@Composable
fun Label(
    @StringRes labelId: Int,
    modifier: Modifier = Modifier,
) =
    Label(
        label = stringResource(id = labelId),
        modifier = modifier
    )

@Composable
fun Label(
    label: String,
    modifier: Modifier = Modifier,
) =
    Text(
        text = label,
        style = MaterialTheme.typography.h6,
        modifier = modifier.padding(8.dp)
    )

@Composable
fun Display(
    text: String,
    modifier: Modifier = Modifier,
) =
    Text(
        text = text,
        style = MaterialTheme.typography.h5,
        modifier = modifier
            .padding(8.dp)
            .padding(start = 16.dp)
    )

@Composable
fun TextField(
    @StringRes labelId: Int,
    @StringRes placeholderId: Int,
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
) =
    OutlinedTextField(
        value = value,
        onValueChange = onValueChange,
        label = {
            Text(text = stringResource(id = labelId))
        },
        placeholder = {
            Text(text = stringResource(id = placeholderId))
        },
        modifier = modifier.fillMaxWidth().padding(8.dp)
    )



@Composable
fun SimpleButton(
    text: String,
    onClick: () -> Unit,
) =
    Button(onClick = onClick) {
        Text(text = text)
    }