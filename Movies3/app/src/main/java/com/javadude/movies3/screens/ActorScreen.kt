package com.javadude.movies3.screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import com.javadude.movies3.Display
import com.javadude.movies3.Label
import com.javadude.movies3.R
import com.javadude.movies3.Screen
import com.javadude.movies3.repo.ActorWithRolesDto
import com.javadude.movies3.repo.MovieDto
import com.javadude.movies3.repo.emptyImmutableList
import com.javadude.movies3.screenTargets
import kotlinx.coroutines.CoroutineScope

@Composable
fun ActorScreen(
    scope: CoroutineScope,
    currentScreen: Screen,
    onScreenSelect: (Screen) -> Unit,
    actorWithRoles: ActorWithRolesDto?,
    select: (MovieDto) -> Unit,
) {
    MovieScaffold(
        scope = scope,
        title = actorWithRoles?.actor?.name ?: stringResource(id = R.string.screen_title_actor),
        topActions = emptyImmutableList(),
        currentScreen = currentScreen,
        screenTargets = screenTargets,
        onScreenSelect = onScreenSelect,
    ) {
        Column(
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) {
            Label(label = stringResource(id = R.string.label_movies_starring_actor, actorWithRoles?.actor?.name ?: ""))

            actorWithRoles?.roles
                ?.forEach {
                    Display(text = it.title, modifier = Modifier.clickable { select(it) })
                }

            repeat(100) {
                Label(label = stringResource(id = R.string.label_movies_starring_actor, actorWithRoles?.actor?.name ?: ""))
            }
        }
    }
}