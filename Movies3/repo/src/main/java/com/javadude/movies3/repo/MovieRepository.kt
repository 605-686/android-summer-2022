package com.javadude.movies3.repo

import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    val ratingsFlow: Flow<List<RatingDto>>
    val moviesFlow: Flow<List<MovieDto>>
    val actorsFlow: Flow<List<ActorDto>>

    suspend fun expand(rating: RatingDto): RatingWithMoviesDto
    suspend fun expand(movie: MovieDto): MovieWithRolesDto
    suspend fun expand(actor: ActorDto): ActorWithRolesDto

    suspend fun update(movie: MovieDto)
    suspend fun update(actor: ActorDto)
    suspend fun update(rating: RatingDto)

    suspend fun deleteMoviesByIds(ids: List<String>)
    suspend fun deleteActorsByIds(ids: List<String>)
    suspend fun deleteRatingsByIds(ids: List<String>)

    suspend fun resetDatabase()
}