package com.javadude.movies3.repo

import android.app.Application
import com.javadude.movies3.data.createDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class MovieDatabaseRepository(private val application: Application): MovieRepository {
    private val dao = createDao(application)

    override val ratingsFlow =
        dao.getRatingsFlow()
            .map { ratings ->
                ImmutableList(
                    ratings.map { it.toDto() }
                )
            }

    override val moviesFlow =
        dao.getMoviesFlow()
            .map { movies ->
                ImmutableList(
                    movies.map { it.toDto() }
                )
            }
    override val actorsFlow =
        dao.getActorsFlow()
            .map { actors ->
                ImmutableList(
                    actors.map { it.toDto() }
                )
            }

    override suspend fun expand(rating: RatingDto): RatingWithMoviesDto = withContext(Dispatchers.IO) {
        dao.getRatingWithMovies(rating.id).toDto()
    }
    override suspend fun expand(movie: MovieDto): MovieWithRolesDto = withContext(Dispatchers.IO) {
        dao.getMovieWithRoles(movie.id).toDto()
    }
    override suspend fun expand(actor: ActorDto): ActorWithRolesDto = withContext(Dispatchers.IO) {
        dao.getActorWithRoles(actor.id).toDto()
    }

    override suspend fun update(movie: MovieDto) = withContext(Dispatchers.IO) {
        dao.update(movie.toEntity())
    }

    override suspend fun update(actor: ActorDto) {
        dao.update(actor.toEntity())
    }

    override suspend fun update(rating: RatingDto) {
        dao.update(rating.toEntity())
    }

    override suspend fun resetDatabase() = withContext(Dispatchers.IO) {
        dao.resetDatabase()
    }

    override suspend fun deleteMoviesByIds(ids: List<String>) = withContext(Dispatchers.IO) {
        dao.deleteMoviesByIds(ids)
    }
    override suspend fun deleteActorsByIds(ids: List<String>) = withContext(Dispatchers.IO) {
        dao.deleteActorsByIds(ids)
    }
    override suspend fun deleteRatingsByIds(ids: List<String>) = withContext(Dispatchers.IO) {
        dao.deleteRatingsByIds(ids)
    }
}