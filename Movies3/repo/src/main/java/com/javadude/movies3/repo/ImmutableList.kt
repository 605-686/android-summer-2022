package com.javadude.movies3.repo

import androidx.compose.runtime.Immutable

@Immutable
data class ImmutableList<T>(val list: List<T>): List<T> by list

fun <T> immutableListOf(vararg items: T) =
    ImmutableList(listOf(*items))

fun <T> emptyImmutableList() = ImmutableList<T>(emptyList())

