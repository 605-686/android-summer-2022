package com.javadude.layouts

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import kotlin.math.max

@Composable
fun BetterFormLayout(
    maxLabelWidthDp: Dp,
    minWidthForSideBySideDp: Dp,
    content: @Composable () -> Unit
) {
    with(LocalDensity.current) {
        val minWidthForSideBySidePx = minWidthForSideBySideDp.toPx()
        val maxLabelWidthPx = maxLabelWidthDp.toPx()

        Layout(
            content = content,
            modifier = Modifier.verticalScroll(rememberScrollState())
        ) { measureables, constraints ->
            check(measureables.size % 2 == 0) { "BetterFormLayout requires an even number of Composables" }

            // measure items
            if (constraints.maxWidth >= minWidthForSideBySidePx) {
                val labelMeasureables = measureables.filterIndexed { n, _ -> n % 2 == 0 }
                val controlsMeasureables = measureables.filterIndexed { n, _ -> n % 2 == 1 }

                val labelConstraints = Constraints(
                    minWidth = 0,
                    minHeight = 0,
                    maxWidth = maxLabelWidthPx.toInt(),
                    maxHeight = Constraints.Infinity,
                )

                val labelPlaceables = labelMeasureables.map {
                    it.measure(labelConstraints)
                }

                val labelsWidth = labelPlaceables.maxOf { it.width }
                val controlsWidth = constraints.maxWidth - labelsWidth

                val controlConstraints = Constraints(
                    minWidth = controlsWidth,
                    minHeight = 0,
                    maxWidth = controlsWidth,
                    maxHeight = Constraints.Infinity,
                )

                val controlPlaceables = controlsMeasureables.map {
                    it.measure(controlConstraints)
                }

                val height = max(
                    labelPlaceables.sumOf { it.height },
                    controlPlaceables.sumOf { it.height },
                )

                layout(constraints.maxWidth, height) {
                    // place items
                    var y = 0
                    labelPlaceables.forEachIndexed { index, label ->
                        val control = controlPlaceables[index]

                        val rowHeight = max(label.height, control.height)
                        val labelOffsetY = (rowHeight - label.height) / 2
                        val controlOffsetY = (rowHeight - control.height) / 2
                        label.placeRelative(0, y + labelOffsetY)
                        control.placeRelative(labelsWidth, y + controlOffsetY)
                        y += rowHeight
                    }
                }

            } else {
                val newConstraints =
                    Constraints(
                        minWidth = constraints.maxWidth,
                        maxWidth = constraints.maxWidth,
                        minHeight = 0,
                        maxHeight = Constraints.Infinity,
                    )

                val placeables = measureables.map {
                    it.measure(newConstraints)
                }

                val height = placeables.sumOf { it.height }

                layout(constraints.maxWidth, height) {
                    var y = 0
                    placeables.forEach {
                        it.placeRelative(0, y)
                        y += it.height
                    }
                }
            }
        }
    }
}

@Composable
fun SampleBorderLayout() {
    BorderLayout(
        modifier = Modifier.fillMaxSize(),
        north = { Text("NORTH", modifier = it.background(Color.Blue).padding(8.dp)) },
        south = { Text("SOUTH", modifier = it.background(Color.Blue).padding(8.dp)) },
        west = { Text("WEST", modifier = it.background(Color.Green).padding(8.dp)) },
        center = { Text("CENTER", modifier = it.background(Color.Red).padding(8.dp)) },
        east = { Text("EAST", modifier = it.background(Color.Green).padding(8.dp)) },
    )
}



@Composable
fun BorderLayout(
    north: @Composable ((Modifier) -> Unit)? = null,
    south: @Composable ((Modifier) -> Unit)? = null,
    east: @Composable ((Modifier) -> Unit)? = null,
    west: @Composable ((Modifier) -> Unit)? = null,
    center: @Composable ((Modifier) -> Unit)? = null,
    modifier: Modifier = Modifier,
) {
    Column(modifier = modifier) {
        north?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        ) {
            west?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }

            center?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .weight(1f)
                ) {
                    it(Modifier.fillMaxSize())
                }
            }

            east?.let {
                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .wrapContentWidth()
                ) {
                    it(Modifier.fillMaxHeight())
                }
            }
        }

        south?.let {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            ) {
                it(Modifier.fillMaxWidth())
            }
        }
    }
}