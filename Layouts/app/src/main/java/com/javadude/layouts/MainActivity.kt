package com.javadude.layouts

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ChainStyle
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstrainedLayoutReference
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintLayoutScope
import androidx.constraintlayout.compose.Dimension
import com.javadude.layouts.ui.theme.LayoutsTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LayoutsTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
//                    ColumnForm()
//                    RowsInColumnForm()
//                    ColumnsInRowForm()
//                    FormUsingConstraintLayout()
//                    FormUsingFormLayout()
                    SampleBorderLayout()
                }
            }
        }
    }
}

@Composable
fun ColumnForm() {
    Column(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        Label("Name")
        Edit(text = "Scott Stanchfield")
        Label("Nickname")
        Edit(text = "Scooter")
        Label("Favorite Dance")
        Edit(text = "Swing")
        Label("Favorite Game")
        Edit(text = "Discs of Tron")
        Label("Age")
        Edit(text = "55")
        Label("Street")
        Edit(text = "123 Sesame")
        Label("City")
        Edit(text = "New York")
        Label("Zip")
        Edit(text = "10101")
    }
}

@Composable
fun ColumnsInRowForm() {
    Row(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        Column {
            Label("Name")
            Label("Nickname")
            Label("Favorite Dance")
            Label("Favorite Game")
            Label("Age")
            Label("Street")
            Label("City")
            Label("Zip")
        }
        Column {
            Edit(text = "Scott Stanchfield")
            Edit(text = "Scooter")
            Edit(text = "Swing")
            Edit(text = "Discs of Tron")
            Edit(text = "55")
            Edit(text = "123 Sesame")
            Edit(text = "New York")
            Edit(text = "10101")
        }
    }
}

@Composable
fun RowsInColumnForm() {
    Column(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        Row {
            Label("Name")
            Edit(text = "Scott Stanchfield")
        }
        Row {
            Label("Nickname")
            Edit(text = "Scooter")
        }
        Row {
            Label("Favorite Dance")
            Edit(text = "Swing")
        }
        Row {
            Label("Favorite Game")
            Edit(text = "Discs of Tron")
        }
        Row {
            Label("Age")
            Edit(text = "55")
        }
        Row {
            Label("Street")
            Edit(text = "123 Sesame")
        }
        Row {
            Label("City")
            Edit(text = "New York")
        }
        Row {
            Label("Zip")
            Edit(text = "10101")
        }
    }
}




@Composable
fun Label(
    text: String,
    modifier: Modifier = Modifier
) {
    Text(
        text = text,
        style = MaterialTheme.typography.h6,
        modifier = modifier.padding(8.dp)
    )
}

@Composable
//fun RowScope.Edit(
fun Edit(
    text: String,
    modifier: Modifier = Modifier
) {
    OutlinedTextField(
        value = text,
        onValueChange = {},
        textStyle = MaterialTheme.typography.h5,
        modifier = modifier
            .padding(8.dp)
            .fillMaxWidth()
//            .weight(1f) // only if used in RowScope or ColumnScope
    )
}

@Composable
//fun RowScope.Edit(
fun BigEdit(
    text: String,
    modifier: Modifier = Modifier
) {
    OutlinedTextField(
        value = text,
        maxLines = 5,
        onValueChange = {},
        textStyle = MaterialTheme.typography.h5,
        modifier = modifier
            .padding(8.dp)
            .fillMaxWidth()
//            .weight(1f) // only if used in RowScope or ColumnScope
    )
}

@Composable
fun FormUsingConstraintLayout() {
    ConstraintLayout(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .wrapContentHeight()
            .verticalScroll(rememberScrollState())
    ) {
        val (l1,l2,l3,l4,l5,l6,l7,e1,e2,e3,e4,e5,e6,e7) = createRefs()

        val barrier = createEndBarrier(l1,l2,l3,l4,l5,l6,l7)

        createVerticalChain(l1,l2,l3,l4,l5,l6,l7, chainStyle = ChainStyle.Spread)
        createVerticalChain(e1,e2,e3,e4,e5,e6,e7, chainStyle = ChainStyle.Spread)

        ConstrainedLabel(l1, "First Name") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e1, "Scott") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
            width = Dimension.fillToConstraints
        }
        ConstrainedLabel(l2, "Last Name") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e2, "Stanchfield") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l3, "Age") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e3, "54") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l4, "Street") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e4, "123 Sesame") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l5, "City") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e5, "New York") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l6, "State") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e6, "NY") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
        ConstrainedLabel(l7, "Zip") {
            start.linkTo(parent.start)
        }
        ConstrainedEdit(e7, "10101") {
            start.linkTo(barrier)
            end.linkTo(parent.end)
        }
    }
}

@Composable
fun ConstraintLayoutScope.ConstrainedLabel(
    ref: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit
) {
    Text(
        text = text,
        style = MaterialTheme.typography.h6,
        modifier = Modifier
            .padding(8.dp)
            .constrainAs(ref, constraintBlock)
    )
}

@Composable
fun ConstraintLayoutScope.ConstrainedEdit(
    ref: ConstrainedLayoutReference,
    text: String,
    constraintBlock: ConstrainScope.() -> Unit
) {
    OutlinedTextField(
        value = text,
        onValueChange = {},
        textStyle = MaterialTheme.typography.h5,
        modifier = Modifier
            .padding(8.dp)
            .constrainAs(ref) {
                constraintBlock()
                width = Dimension.fillToConstraints
            }
    )
}


@Composable
fun FormUsingFormLayout() {
    BetterFormLayout(maxLabelWidthDp = 200.dp, minWidthForSideBySideDp = 700.dp) {
        Label("First Name")
        Edit("Scott")
        Label("Last Name")
        Edit("Stanchfield")
        Label("Age")
        Edit("54")
        Label("Street")
        BigEdit("123 Sesame\nFoo Foo\nFee fee")
        Label("City")
        Edit("New York")
        Label("State")
        Edit("NY")
        Label("Zip")
        Edit("10101")
    }
}