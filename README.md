# Android Summer 2022

## Videos

   * Week 1: https://youtu.be/4-YnVanewaY

   * Week 2: https://youtu.be/EITm6SJ6ZlA

   * Week 3: https://youtu.be/ritxSni78IQ

   * Week 4: https://youtu.be/fTi6F2UKDO4 

   * Week 5: https://youtu.be/-24DChAjaoQ 

   * Week 6: Instructor could not speak well... Please see
   
      * https://gitlab.com/stanchfield-android-development-2021-refresh/android-development/-/tree/main/modules/compose-graphics-1 (Introduction and Graph Editor only)

      * https://gitlab.com/stanchfield-android-development-2021-refresh/android-development/-/tree/main/modules/compose-graphics-2 (Both videos)
   

   * Week 7: https://youtu.be/Nsf4rGvs7TY

   * Week 8: https://youtu.be/0cwvfLQOJIE

   * Week 9: https://youtu.be/QjDJBrK9_Cs
   
   * Week 10: Instructor not feeling well. Many apologies! Please watch all videos in

      * https://gitlab.com/stanchfield-android-development-2021-refresh/android-development/-/tree/main/modules/testing 

   * Week 11: No students showed up for the live session; closed session after 20 mins. (That's a first!)
     Please watch the videos in these modules:

      * https://gitlab.com/stanchfield-android-development-2021-refresh/android-development/-/tree/main/modules/nfc

      * https://gitlab.com/stanchfield-android-development-2021-refresh/android-development/-/tree/main/modules/widgets

   * Week 12
   
      * Live session: https://youtu.be/eNnskaycbQ4

      * Publishing: https://gitlab.com/stanchfield-android-development-2021-refresh/android-development/-/tree/main/modules/publishing
